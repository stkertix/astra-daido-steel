var data = {

	customerCard: {
		raw: [
			{	id:'1',	name:'PT Astra Otoparts Tbk.',									address:'Jl. pegangsaan dua, kelapa gading',				product:'Gear',				area:'DKI Jakarta',	detail:'<button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal"><span class="glyphicon glyphicon-folder-open"></span></button>',	},
			{	id:'2',	name:'Asno Horie Indonesia, PT',								address:'Kedoya Raya',				product:'Rantai',			area:'Bogor',		detail:'<button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal"><span class="glyphicon glyphicon-folder-open"></span></button>',	},
			{	id:'3',	name:'FCC Indonesia, PT',										address:'Jalan Panjang',			product:'Body Motor',		area:'Tanggerang',	detail:'<button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal"><span class="glyphicon glyphicon-folder-open"></span></button>',	},
			{	id:'4',	name:'Asianet Spring Indonesia, PT',							address:'Srengseng',				product:'velg',				area:'Cikarang',	detail:'<button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal"><span class="glyphicon glyphicon-folder-open"></span></button>',	},
			{	id:'5',	name:'Nusantara Buana Sakti, PT',								address:'Joglo',					product:'Pintu Mobil',		area:'Bogor',		detail:'<button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal"><span class="glyphicon glyphicon-folder-open"></span></button>',	},
			{	id:'6',	name:'Topas Multifinance, PT',									address:'Meruya',					product:'Jok Motor',		area:'Bekasi',		detail:'<button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal"><span class="glyphicon glyphicon-folder-open"></span></button>',	},
			{	id:'7',	name:'Selamat Sempana Perkasa, PT',								address:'Cengkareng',				product:'Knalpot',			area:'Bandung',		detail:'<button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal"><span class="glyphicon glyphicon-folder-open"></span></button>',	},
			{	id:'8',	name:'Ichikoh Indonesia, PT',									address:'Grogol',					product:'Knalpot Racing',	area:'DKI Jakarta',	detail:'<button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal"><span class="glyphicon glyphicon-folder-open"></span></button>',	},
			{	id:'9',	name:'IRC Inoac Indonesia, PT [Factory]',						address:'Petamburan',				product:'Shock',			area:'Bogor',		detail:'<button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal"><span class="glyphicon glyphicon-folder-open"></span></button>',	},
			{	id:'10',	name:'Rema Tip Top Indonesia, PT',								address:'Palmerah',					product:'Gear, Rantai',		area:'Tanggerang',	detail:'<button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal"><span class="glyphicon glyphicon-folder-open"></span></button>',	},
			{	id:'11',	name:'Fondanusa Aditama, PT',									address:'Kemanggisan',				product:'Speedo meter',		area:'Cikarang',	detail:'<button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal"><span class="glyphicon glyphicon-folder-open"></span></button>',	},
			{	id:'12',	name:'Ingress Malindo Ventures, PT',							address:'Slipi',					product:'Gear',				area:'Bogor',		detail:'<button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal"><span class="glyphicon glyphicon-folder-open"></span></button>',	},
			{	id:'13',	name:'Enam Lima Rubber Industri, PT',							address:'Rawa Buaya',				product:'Rantai',			area:'Bekasi',		detail:'<button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal"><span class="glyphicon glyphicon-folder-open"></span></button>',	},
			{	id:'14',	name:'Frina Lestari Nusantara, PT',								address:'Mangga Besar',				product:'Body Motor',		area:'Bandung',		detail:'<button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal"><span class="glyphicon glyphicon-folder-open"></span></button>',	},
			{	id:'15',	name:'Prima Sarana Usaha, PT',									address:'Tubagus Angke',			product:'velg',				area:'Bogor',		detail:'<button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal"><span class="glyphicon glyphicon-folder-open"></span></button>',	},
			{	id:'16',	name:'Kaneta Indonesia, PT',									address:'Kali Deres',				product:'Pintu Mobil',		area:'Bekasi',		detail:'<button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal"><span class="glyphicon glyphicon-folder-open"></span></button>',	},
			{	id:'17',	name:'Wijara Nagatsupazki, PT',									address:'Petojo',					product:'Jok Motor',		area:'Bandung',		detail:'<button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal"><span class="glyphicon glyphicon-folder-open"></span></button>',	},
		],
		grid: function() {
			var result = [];
			$.each(data.customerCard.raw, function(k, v) {
				var arr = $.map(v, function(el) { return el; });
				result.push(arr);
			});

			return result;
		}
	},


	quotation: {
		raw: [					
			{	id:'5673668827',	noQuo:'QUO/2016/89811',	customer:'Dharma Controlcable Indonesia, PT',	status:'Cancel',	dueDate:'2016-11-01',	},
			{	id:'8127791419',	noQuo:'QUO/2016/41648',	customer:'Asno Horie Indonesia, PT',	status:'OK',	dueDate:'2016-11-01',	},
			{	id:'4203073246',	noQuo:'QUO/2016/88998',	customer:'FCC Indonesia, PT',	status:'OK',	dueDate:'2016-11-01',	},
			{	id:'4036493702',	noQuo:'QUO/2016/27812',	customer:'Asianet Spring Indonesia, PT',	status:'OK',	dueDate:'2016-11-01',	},
			{	id:'3437122548',	noQuo:'QUO/2016/97288',	customer:'Nusantara Buana Sakti, PT',	status:'Pending',	dueDate:'2016-11-01',	},
			{	id:'2182455654',	noQuo:'QUO/2016/43075',	customer:'Topas Multifinance, PT',	status:'OK',	dueDate:'2016-11-01',	},
			{	id:'4705844123',	noQuo:'QUO/2016/33890',	customer:'Selamat Sempana Perkasa, PT',	status:'OK',	dueDate:'2016-11-01',	},
			{	id:'5792685732',	noQuo:'QUO/2016/72535',	customer:'Ichikoh Indonesia, PT',	status:'OK',	dueDate:'2016-11-01',	},
			{	id:'4366156052',	noQuo:'QUO/2016/53463',	customer:'IRC Inoac Indonesia, PT [Factory]',	status:'OK',	dueDate:'2016-11-01',	},
			{	id:'6801839769',	noQuo:'QUO/2016/50600',	customer:'Rema Tip Top Indonesia, PT',	status:'OK',	dueDate:'2016-11-01',	},
			{	id:'9114681677',	noQuo:'QUO/2016/68083',	customer:'Fondanusa Aditama, PT',	status:'OK',	dueDate:'2016-11-01',	},
			{	id:'3230860792',	noQuo:'QUO/2016/13224',	customer:'Ingress Malindo Ventures, PT',	status:'OK',	dueDate:'2016-11-01',	},
			{	id:'1670074102',	noQuo:'QUO/2016/10359',	customer:'Enam Lima Rubber Industri, PT',	status:'Pending',	dueDate:'2016-11-01',	},
			{	id:'1668095872',	noQuo:'QUO/2016/49490',	customer:'Frina Lestari Nusantara, PT',	status:'Pending',	dueDate:'2016-11-01',	},
			{	id:'4186769113',	noQuo:'QUO/2016/67355',	customer:'Prima Sarana Usaha, PT',	status:'Pending',	dueDate:'2016-11-01',	},
			{	id:'4393621644',	noQuo:'QUO/2016/93987',	customer:'Kaneta Indonesia, PT',	status:'Pending',	dueDate:'2016-11-01',	},
			{	id:'6257699640',	noQuo:'QUO/2016/94758',	customer:'Wijara Nagatsupazki, PT',	status:'OK',	dueDate:'2016-11-01',	},
			{	id:'6355622595',	noQuo:'QUO/2016/25022',	customer:'Mulia Industrindo Tbk., PT',	status:'OK',	dueDate:'2016-11-01',	},
			{	id:'6807391722',	noQuo:'QUO/2016/10868',	customer:'Prapat Jaya',	status:'OK',	dueDate:'2016-11-01',	},
			{	id:'1528924208',	noQuo:'QUO/2016/22393',	customer:'Sinar Agung Pemuda, PT',	status:'OK',	dueDate:'2016-11-01',	},
			{	id:'1997421922',	noQuo:'QUO/2016/82852',	customer:'JRD Bright Motor Assembler, PT',	status:'OK',	dueDate:'2016-11-01',	},
			{	id:'2526814781',	noQuo:'QUO/2016/63671',	customer:'JRD Finance Utama, PT',	status:'Pending',	dueDate:'2016-11-01',	},
			{	id:'3029699695',	noQuo:'QUO/2016/37942',	customer:'Herman Interco, PT',	status:'OK',	dueDate:'2016-11-01',	},
			{	id:'6909488524',	noQuo:'QUO/2016/37657',	customer:'Astra International Tbk., PT',	status:'OK',	dueDate:'2016-11-01',	},
			{	id:'5239716380',	noQuo:'QUO/2016/34015',	customer:'Nachi Indonesia, PT',	status:'OK',	dueDate:'2016-11-01',	},
			{	id:'2293372951',	noQuo:'QUO/2016/67053',	customer:'Rabana Investindo, PT',	status:'Pending',	dueDate:'2016-11-01',	},
			{	id:'7105377126',	noQuo:'QUO/2016/59697',	customer:'Panata Jaya Mandiri, PT [Factory]',	status:'OK',	dueDate:'2016-11-01',	},
			{	id:'4399789966',	noQuo:'QUO/2016/44157',	customer:'Ikatan Ahli Teknik Otomotive (IATO)',	status:'Pending',	dueDate:'2016-11-01',	},
			{	id:'1531440916',	noQuo:'QUO/2016/95165',	customer:'Melindo Cipta Agung, PT',	status:'OK',	dueDate:'2016-11-01',	},
			{	id:'7974029881',	noQuo:'QUO/2016/85905',	customer:'Gabungan Industri Alat-alat Mobil dan Motor (GIAMM)',	status:'OK',	dueDate:'2016-11-01',	},
			{	id:'2814543033',	noQuo:'QUO/2016/17508',	customer:'Mutindo Bumi Persada, PT',	status:'Pending',	dueDate:'2016-11-01',	},
			{	id:'2332462319',	noQuo:'QUO/2016/42113',	customer:'Intertruck Parts, PD',	status:'OK',	dueDate:'2016-11-01',	},
			{	id:'6035697737',	noQuo:'QUO/2016/27900',	customer:'Panata Jaya Mandiri, PT',	status:'Cancel',	dueDate:'2016-11-01',	},
			{	id:'4710121571',	noQuo:'QUO/2016/90395',	customer:'Selamat Sempurna Tbk., PT',	status:'Cancel',	dueDate:'2016-11-01',	},
			{	id:'6012877940',	noQuo:'QUO/2016/47018',	customer:'Martena Mccray, PT',	status:'OK',	dueDate:'2016-11-01',	},
		],								
		grid: function() {
			var result = [];
			$.each(data.quotation.raw, function(k, v) {
				var arr = $.map(v, function(el) { return el; });
				result.push(arr);
			});

			return result;
		}
	},


	addquotation: {
		raw: [					
			{	itempermintaan:'DC11-F 10 x 100 x 200',	itempenawaran:'DC11-F 13 x 100 x 200',	spesification:'Cutting - Machining',					pcs:'5',	kg:'90',	unitprice:'180000',	discount:'10',	subtotal:'14.580.000',	material:'DC11-F', thickness:10, width:100, length:200,},
			{	itempermintaan:'DC53-F 15 x 200 x 300',	itempenawaran:'DC53-F 15 x 200 x 300',	spesification:'Cutting - Machining - Heat Treatment',	pcs:'7',	kg:'150',	unitprice:'230000', discount:'20',	subtotal:'27.600.000',	material:'DC53-F', thickness:15, width:200, length:300,},
			{	itempermintaan:'DC11-R dia 46 x 200',	itempenawaran:'DC11-R dia 46 x 200',	spesification:'Cutting',								pcs:'5',	kg:'100',	unitprice:'190000', discount:'12',	subtotal:'16.720.000',	material:'DC11-R', thickness:46, width:0, length:200,},
		],								
		grid: function() {
			var result = [];
			$.each(data.addquotation.raw, function(k, v) {
				var arr = $.map(v, function(el) { return el; });
				result.push(arr);
			});

			return result;
		}
	},


	salesOrder: {
		raw: [					
			{	id:'5673668827',	noSo:'SO/2016/89811',	customer:'PT Astra Otoparts Tbk.',	status:'Cancel',	dueDate:'2016-11-01',	},
			{	id:'8127791419',	noSo:'SO/2016/41648',	customer:'Asno Horie Indonesia, PT',	status:'OK',	dueDate:'2016-11-01',	},
			{	id:'4203073246',	noSo:'SO/2016/88998',	customer:'FCC Indonesia, PT',	status:'OK',	dueDate:'2016-11-01',	},
			{	id:'4036493702',	noSo:'SO/2016/27812',	customer:'Asianet Spring Indonesia, PT',	status:'OK',	dueDate:'2016-11-01',	},
			{	id:'3437122548',	noSo:'SO/2016/97288',	customer:'Nusantara Buana Sakti, PT',	status:'Pending',	dueDate:'2016-11-01',	},
			{	id:'2182455654',	noSo:'SO/2016/43075',	customer:'Topas Multifinance, PT',	status:'OK',	dueDate:'2016-11-01',	},
			{	id:'4705844123',	noSo:'SO/2016/33890',	customer:'Selamat Sempana Perkasa, PT',	status:'OK',	dueDate:'2016-11-01',	},
			{	id:'5792685732',	noSo:'SO/2016/72535',	customer:'Ichikoh Indonesia, PT',	status:'OK',	dueDate:'2016-11-01',	},
			{	id:'4366156052',	noSo:'SO/2016/53463',	customer:'IRC Inoac Indonesia, PT [Factory]',	status:'OK',	dueDate:'2016-11-01',	},
			{	id:'6801839769',	noSo:'SO/2016/50600',	customer:'Rema Tip Top Indonesia, PT',	status:'OK',	dueDate:'2016-11-01',	},
			{	id:'9114681677',	noSo:'SO/2016/68083',	customer:'Fondanusa Aditama, PT',	status:'OK',	dueDate:'2016-11-01',	},
			{	id:'3230860792',	noSo:'SO/2016/13224',	customer:'Ingress Malindo Ventures, PT',	status:'OK',	dueDate:'2016-11-01',	},
			{	id:'1670074102',	noSo:'SO/2016/10359',	customer:'Enam Lima Rubber Industri, PT',	status:'Pending',	dueDate:'2016-11-01',	},
			{	id:'1668095872',	noSo:'SO/2016/49490',	customer:'Frina Lestari Nusantara, PT',	status:'Pending',	dueDate:'2016-11-01',	},
			{	id:'4186769113',	noSo:'SO/2016/67355',	customer:'Prima Sarana Usaha, PT',	status:'Pending',	dueDate:'2016-11-01',	},
			{	id:'4393621644',	noSo:'SO/2016/93987',	customer:'Kaneta Indonesia, PT',	status:'Pending',	dueDate:'2016-11-01',	},
			{	id:'6257699640',	noSo:'SO/2016/94758',	customer:'Wijara Nagatsupazki, PT',	status:'OK',	dueDate:'2016-11-01',	},
			{	id:'6355622595',	noSo:'SO/2016/25022',	customer:'Mulia Industrindo Tbk., PT',	status:'OK',	dueDate:'2016-11-01',	},
			{	id:'6807391722',	noSo:'SO/2016/10868',	customer:'Prapat Jaya',	status:'OK',	dueDate:'2016-11-01',	},
			{	id:'1528924208',	noSo:'SO/2016/22393',	customer:'Sinar Agung Pemuda, PT',	status:'OK',	dueDate:'2016-11-01',	},
			{	id:'1997421922',	noSo:'SO/2016/82852',	customer:'JRD Bright Motor Assembler, PT',	status:'OK',	dueDate:'2016-11-01',	},
			{	id:'2526814781',	noSo:'SO/2016/63671',	customer:'JRD Finance Utama, PT',	status:'Pending',	dueDate:'2016-11-01',	},
			{	id:'3029699695',	noSo:'SO/2016/37942',	customer:'Herman Interco, PT',	status:'OK',	dueDate:'2016-11-01',	},
			{	id:'6909488524',	noSo:'SO/2016/37657',	customer:'Astra International Tbk., PT',	status:'OK',	dueDate:'2016-11-01',	},
			{	id:'5239716380',	noSo:'SO/2016/34015',	customer:'Nachi Indonesia, PT',	status:'OK',	dueDate:'2016-11-01',	},
			{	id:'2293372951',	noSo:'SO/2016/67053',	customer:'Rabana Investindo, PT',	status:'Pending',	dueDate:'2016-11-01',	},
			{	id:'7105377126',	noSo:'SO/2016/59697',	customer:'Panata Jaya Mandiri, PT [Factory]',	status:'OK',	dueDate:'2016-11-01',	},
			{	id:'4399789966',	noSo:'SO/2016/44157',	customer:'Ikatan Ahli Teknik Otomotive (IATO)',	status:'Pending',	dueDate:'2016-11-01',	},
			{	id:'1531440916',	noSo:'SO/2016/95165',	customer:'Melindo Cipta Agung, PT',	status:'OK',	dueDate:'2016-11-01',	},
			{	id:'7974029881',	noSo:'SO/2016/85905',	customer:'Gabungan Industri Alat-alat Mobil dan Motor (GIAMM)',	status:'OK',	dueDate:'2016-11-01',	},
			{	id:'2814543033',	noSo:'SO/2016/17508',	customer:'Mutindo Bumi Persada, PT',	status:'Pending',	dueDate:'2016-11-01',	},
			{	id:'2332462319',	noSo:'SO/2016/42113',	customer:'Intertruck Parts, PD',	status:'OK',	dueDate:'2016-11-01',	},
			{	id:'6035697737',	noSo:'SO/2016/27900',	customer:'Panata Jaya Mandiri, PT',	status:'Cancel',	dueDate:'2016-11-01',	},
			{	id:'4710121571',	noSo:'SO/2016/90395',	customer:'Selamat Sempurna Tbk., PT',	status:'Cancel',	dueDate:'2016-11-01',	},
			{	id:'6012877940',	noSo:'SO/2016/47018',	customer:'Martena Mccray, PT',	status:'OK',	dueDate:'2016-11-01',	},
		],								
		grid: function() {
			var result = [];
			$.each(data.salesOrder.raw, function(k, v) {
				var arr = $.map(v, function(el) { return el; });
				result.push(arr);
			});

			return result;
		}
	},


	inquiry: {
		raw: [					
			{	id:'5673668827',	customer:'Dharma Controlcable Indonesia, PT',					sales:'Rangga',		material:'DC11',	tebal:'55',	lebar:'39',	diameter:'87',	pcs:'69',	kg:'200',	status:'Pending',	},
			{	id:'8127791419',	customer:'Asno Horie Indonesia, PT',							sales:'Sigit',		material:'DC11',	tebal:'75',	lebar:'19',	diameter:'55',	pcs:'78',	kg:'200',	status:'OK',	},
			{	id:'4203073246',	customer:'FCC Indonesia, PT',									sales:'Tati',		material:'DC53',	tebal:'86',	lebar:'38',	diameter:'38',	pcs:'17',	kg:'200',	status:'OK',	},
			{	id:'4036493702',	customer:'Asianet Spring Indonesia, PT',						sales:'Ijah',		material:'GOA',	tebal:'88',	lebar:'19',	diameter:'4',		pcs:'86',	kg:'200',	status:'OK',	},
			{	id:'3437122548',	customer:'Nusantara Buana Sakti, PT',							sales:'Innah',		material:'DHA1',	tebal:'48',	lebar:'35',	diameter:'56',	pcs:'18',	kg:'200',	status:'Pending',	},
			{	id:'2182455654',	customer:'Topas Multifinance, PT',								sales:'Tukiem',		material:'DC11',	tebal:'73',	lebar:'41',	diameter:'16',	pcs:'29',	kg:'200',	status:'OK',	},
			{	id:'4705844123',	customer:'Selamat Sempana Perkasa, PT',							sales:'Suketi',		material:'DC53',	tebal:'1',	lebar:'57',	diameter:'48',	pcs:'4',	kg:'200',	status:'OK',	},
			{	id:'5792685732',	customer:'Ichikoh Indonesia, PT',								sales:'Firman',		material:'GOA',	tebal:'34',	lebar:'10',	diameter:'32',		pcs:'34',	kg:'200',	status:'OK',	},
			{	id:'4366156052',	customer:'IRC Inoac Indonesia, PT [Factory]',					sales:'Jaja',		material:'DC11',	tebal:'55',	lebar:'31',	diameter:'93',	pcs:'55',	kg:'400',	status:'OK',	},
			{	id:'6801839769',	customer:'Rema Tip Top Indonesia, PT',							sales:'Ujang',		material:'DC53',	tebal:'40',	lebar:'37',	diameter:'59',	pcs:'1',	kg:'300',	status:'OK',	},
			{	id:'9114681677',	customer:'Fondanusa Aditama, PT',								sales:'Rangga',		material:'DC11',	tebal:'9',	lebar:'67',	diameter:'6',	pcs:'39',	kg:'450',	status:'OK',	},
			{	id:'3230860792',	customer:'Ingress Malindo Ventures, PT',						sales:'Sigit',		material:'DC11',	tebal:'19',	lebar:'35',	diameter:'98',	pcs:'13',	kg:'200',	status:'OK',	},
			{	id:'1670074102',	customer:'Enam Lima Rubber Industri, PT',						sales:'Tati',		material:'DC53',	tebal:'86',	lebar:'62',	diameter:'1',	pcs:'49',	kg:'550',	status:'Pending',	},
			{	id:'1668095872',	customer:'Frina Lestari Nusantara, PT',							sales:'Ijah',		material:'GOA',	tebal:'58',	lebar:'86',	diameter:'54',		pcs:'61',	kg:'200',	status:'Pending',	},
			{	id:'4186769113',	customer:'Prima Sarana Usaha, PT',								sales:'Innah',		material:'DC53',	tebal:'13',	lebar:'42',	diameter:'27',	pcs:'11',	kg:'200',	status:'Pending',	},
			{	id:'4393621644',	customer:'Kaneta Indonesia, PT',								sales:'Tukiem',		material:'DC53',	tebal:'43',	lebar:'99',	diameter:'61',	pcs:'58',	kg:'340',	status:'Pending',	},
			{	id:'6257699640',	customer:'Wijara Nagatsupazki, PT',								sales:'Suketi',		material:'DC53',	tebal:'82',	lebar:'73',	diameter:'91',	pcs:'14',	kg:'540',	status:'OK',	},
			{	id:'6355622595',	customer:'Mulia Industrindo Tbk., PT',							sales:'Firman',		material:'GOA',	tebal:'69',	lebar:'37',	diameter:'98',		pcs:'28',	kg:'240',	status:'OK',	},
			{	id:'6807391722',	customer:'Prapat Jaya',											sales:'Jaja',		material:'DHA1',	tebal:'18',	lebar:'20',	diameter:'7',	pcs:'48',	kg:'200',	status:'OK',	},
			{	id:'1528924208',	customer:'Sinar Agung Pemuda, PT',								sales:'Ujang',		material:'DC11',	tebal:'39',	lebar:'74',	diameter:'83',	pcs:'17',	kg:'200',	status:'OK',	},
			{	id:'1997421922',	customer:'JRD Bright Motor Assembler, PT',						sales:'Rangga',		material:'DC11',	tebal:'24',	lebar:'67',	diameter:'60',	pcs:'85',	kg:'200',	status:'OK',	},
			{	id:'2526814781',	customer:'JRD Finance Utama, PT',								sales:'Sigit',		material:'DHA1',	tebal:'70',	lebar:'48',	diameter:'56',	pcs:'40',	kg:'200',	status:'Pending',	},
			{	id:'3029699695',	customer:'Herman Interco, PT',									sales:'Tati',		material:'DC53',	tebal:'64',	lebar:'92',	diameter:'27',	pcs:'84',	kg:'200',	status:'OK',	},
			{	id:'6909488524',	customer:'Astra International Tbk., PT',						sales:'Ijah',		material:'DC53',	tebal:'42',	lebar:'30',	diameter:'77',	pcs:'8',	kg:'200',	status:'OK',	},
			{	id:'5239716380',	customer:'Nachi Indonesia, PT',									sales:'Innah',		material:'GOA',	tebal:'26',	lebar:'55',	diameter:'100',		pcs:'65',	kg:'200',	status:'OK',	},
			{	id:'2293372951',	customer:'Rabana Investindo, PT',								sales:'Tukiem',		material:'DHA1',	tebal:'43',	lebar:'91',	diameter:'78',	pcs:'17',	kg:'200',	status:'Pending',	},
			{	id:'7105377126',	customer:'Panata Jaya Mandiri, PT [Factory]',					sales:'Suketi',		material:'DC11',	tebal:'48',	lebar:'1',	diameter:'77',	pcs:'63',	kg:'200',	status:'OK',	},
			{	id:'4399789966',	customer:'Ikatan Ahli Teknik Otomotive (IATO)',					sales:'Firman',		material:'DHA1',	tebal:'86',	lebar:'99',	diameter:'4',	pcs:'8',	kg:'200',	status:'Pending',	},
			{	id:'1531440916',	customer:'Melindo Cipta Agung, PT',								sales:'Jaja',		material:'DC53',	tebal:'20',	lebar:'7',	diameter:'56',	pcs:'69',	kg:'200',	status:'OK',	},
			{	id:'7974029881',	customer:'Gabungan Industri Alat-alat Mobil dan Motor (GIAMM)',	sales:'Ujang',		material:'DC53',	tebal:'95',	lebar:'86',	diameter:'88',	pcs:'64',	kg:'200',	status:'OK',	},
			{	id:'2814543033',	customer:'Mutindo Bumi Persada, PT',							sales:'Innah',		material:'DC53',	tebal:'57',	lebar:'83',	diameter:'99',	pcs:'92',	kg:'200',	status:'Pending',	},
			{	id:'2332462319',	customer:'Intertruck Parts, PD',								sales:'Tukiem',		material:'DC53',	tebal:'19',	lebar:'72',	diameter:'60',	pcs:'45',	kg:'200',	status:'OK',	},
			{	id:'6035697737',	customer:'Panata Jaya Mandiri, PT',								sales:'Suketi',		material:'DHA1',	tebal:'86',	lebar:'46',	diameter:'39',	pcs:'47',	kg:'200',	status:'Pending',	},
			{	id:'4710121571',	customer:'Selamat Sempurna Tbk., PT',							sales:'Firman',		material:'DHA1',	tebal:'27',	lebar:'22',	diameter:'55',	pcs:'49',	kg:'200',	status:'Pending',	},
			{	id:'6012877940',	customer:'Martena Mccray, PT',									sales:'Jaja',		material:'GOA',	tebal:'38',	lebar:'73',	diameter:'58',		pcs:'43',	kg:'200',	status:'OK',	},
		],								
		grid: function() {
			var result = [];
			$.each(data.inquiry.raw, function(k, v) {
				var arr = $.map(v, function(el) { return el; });
				result.push(arr);
			});

			return result;
		}
	},


	workOrder: {
		raw: [								
			{	id:'5849300794',	noWo:'WO/2016/22269',	noSo:'SO/2016/89811',	noPo:'314913681',	customer:'Dharma Controlcable Indonesia, PT',	specification:'Cutting',	status:'Cutting',	datetime:'13-10-2016 15:45',	},
			{	id:'5133105697',	noWo:'WO/2016/12585',	noSo:'SO/2016/41648',	noPo:'907908777',	customer:'Asno Horie Indonesia, PT',	specification:'Cutting',	status:'Cutting',	datetime:'13-10-2016 7:34',	},
			{	id:'3756993868',	noWo:'WO/2016/82039',	noSo:'SO/2016/88998',	noPo:'589466620',	customer:'FCC Indonesia, PT',	specification:'Cutting - Machining - Heat Treatment',	status:'Heat Treatment',	datetime:'13-10-2016 11:34',	},
			{	id:'5870833307',	noWo:'WO/2016/31657',	noSo:'SO/2016/27812',	noPo:'265402666',	customer:'Asianet Spring Indonesia, PT',	specification:'Cutting',	status:'Cutting',	datetime:'13-10-2016 18:2',	},
			{	id:'3652968870',	noWo:'WO/2016/14834',	noSo:'SO/2016/97288',	noPo:'490063151',	customer:'Nusantara Buana Sakti, PT',	specification:'Cutting',	status:'Cutting',	datetime:'13-10-2016 7:41',	},
			{	id:'8657251926',	noWo:'WO/2016/19857',	noSo:'SO/2016/43075',	noPo:'833057129',	customer:'Topas Multifinance, PT',	specification:'Cutting',	status:'Cutting',	datetime:'13-10-2016 20:1',	},
			{	id:'8924586113',	noWo:'WO/2016/60719',	noSo:'SO/2016/33890',	noPo:'201525796',	customer:'Selamat Sempana Perkasa, PT',	specification:'Cutting - Machining',	status:'Machining',	datetime:'13-10-2016 16:30',	},
			{	id:'3406830926',	noWo:'WO/2016/36968',	noSo:'SO/2016/72535',	noPo:'723353328',	customer:'Ichikoh Indonesia, PT',	specification:'Cutting - Machining',	status:'Machining',	datetime:'13-10-2016 1:0',	},
			{	id:'8339683973',	noWo:'WO/2016/49717',	noSo:'SO/2016/53463',	noPo:'449925771',	customer:'IRC Inoac Indonesia, PT [Factory]',	specification:'Cutting - Machining - Heat Treatment',	status:'Heat Treatment',	datetime:'13-10-2016 14:12',	},
			{	id:'6346667037',	noWo:'WO/2016/42058',	noSo:'SO/2016/50600',	noPo:'693525000',	customer:'Rema Tip Top Indonesia, PT',	specification:'Cutting',	status:'Cutting',	datetime:'13-10-2016 10:10',	},
			{	id:'5988650190',	noWo:'WO/2016/60188',	noSo:'SO/2016/68083',	noPo:'165778860',	customer:'Fondanusa Aditama, PT',	specification:'Cutting - Machining',	status:'Machining',	datetime:'13-10-2016 7:52',	},
			{	id:'6662486232',	noWo:'WO/2016/77640',	noSo:'SO/2016/13224',	noPo:'796825676',	customer:'Ingress Malindo Ventures, PT',	specification:'Cutting - Machining - Heat Treatment',	status:'Heat Treatment',	datetime:'13-10-2016 0:43',	},
			{	id:'9635649043',	noWo:'WO/2016/40553',	noSo:'SO/2016/10359',	noPo:'849660347',	customer:'Enam Lima Rubber Industri, PT',	specification:'Cutting - Machining',	status:'Machining',	datetime:'13-10-2016 8:53',	},
			{	id:'6790797824',	noWo:'WO/2016/11712',	noSo:'SO/2016/49490',	noPo:'806230213',	customer:'Frina Lestari Nusantara, PT',	specification:'Cutting - Machining - Heat Treatment',	status:'Heat Treatment',	datetime:'13-10-2016 1:48',	},
			{	id:'9784311708',	noWo:'WO/2016/66896',	noSo:'SO/2016/67355',	noPo:'772179718',	customer:'Prima Sarana Usaha, PT',	specification:'Cutting - Machining',	status:'Machining',	datetime:'13-10-2016 22:40',	},
			{	id:'8129734759',	noWo:'WO/2016/37880',	noSo:'SO/2016/93987',	noPo:'906034183',	customer:'Kaneta Indonesia, PT',	specification:'Cutting - Machining - Heat Treatment',	status:'Heat Treatment',	datetime:'13-10-2016 15:11',	},
			{	id:'4227379122',	noWo:'WO/2016/71680',	noSo:'SO/2016/94758',	noPo:'146487235',	customer:'Wijara Nagatsupazki, PT',	specification:'Cutting - Machining - Heat Treatment',	status:'Heat Treatment',	datetime:'13-10-2016 11:46',	},
			{	id:'1095545218',	noWo:'WO/2016/30316',	noSo:'SO/2016/25022',	noPo:'689539003',	customer:'Mulia Industrindo Tbk., PT',	specification:'Cutting - Machining',	status:'Machining',	datetime:'13-10-2016 12:50',	},
			{	id:'2267445826',	noWo:'WO/2016/27007',	noSo:'SO/2016/10868',	noPo:'826651138',	customer:'Prapat Jaya',	specification:'Cutting - Machining - Heat Treatment',	status:'Heat Treatment',	datetime:'13-10-2016 16:15',	},
			{	id:'3860896262',	noWo:'WO/2016/55330',	noSo:'SO/2016/22393',	noPo:'423533268',	customer:'Sinar Agung Pemuda, PT',	specification:'Cutting - Machining - Heat Treatment',	status:'Heat Treatment',	datetime:'13-10-2016 10:26',	},
			{	id:'1878364168',	noWo:'WO/2016/57749',	noSo:'SO/2016/82852',	noPo:'137798455',	customer:'JRD Bright Motor Assembler, PT',	specification:'Cutting',	status:'Cutting',	datetime:'13-10-2016 19:16',	},
			{	id:'5651250107',	noWo:'WO/2016/98644',	noSo:'SO/2016/63671',	noPo:'229759641',	customer:'JRD Finance Utama, PT',	specification:'Cutting - Machining',	status:'Machining',	datetime:'13-10-2016 15:15',	},
			{	id:'9030548108',	noWo:'WO/2016/91679',	noSo:'SO/2016/37942',	noPo:'998929976',	customer:'Herman Interco, PT',	specification:'Cutting - Machining',	status:'Machining',	datetime:'13-10-2016 10:58',	},
			{	id:'8788041259',	noWo:'WO/2016/60816',	noSo:'SO/2016/37657',	noPo:'704342889',	customer:'Astra International Tbk., PT',	specification:'Cutting - Machining - Heat Treatment',	status:'Heat Treatment',	datetime:'13-10-2016 2:12',	},
			{	id:'1485502254',	noWo:'WO/2016/94025',	noSo:'SO/2016/34015',	noPo:'636664825',	customer:'Nachi Indonesia, PT',	specification:'Cutting - Machining',	status:'Machining',	datetime:'13-10-2016 5:36',	},
			{	id:'6449287682',	noWo:'WO/2016/19390',	noSo:'SO/2016/67053',	noPo:'746222444',	customer:'Rabana Investindo, PT',	specification:'Cutting - Machining',	status:'Machining',	datetime:'13-10-2016 1:5',	},
			{	id:'2976820013',	noWo:'WO/2016/74301',	noSo:'SO/2016/59697',	noPo:'946282363',	customer:'Panata Jaya Mandiri, PT [Factory]',	specification:'Cutting',	status:'Cutting',	datetime:'13-10-2016 8:40',	},
			{	id:'6020491335',	noWo:'WO/2016/69609',	noSo:'SO/2016/44157',	noPo:'927483732',	customer:'Ikatan Ahli Teknik Otomotive (IATO)',	specification:'Cutting - Machining',	status:'Machining',	datetime:'13-10-2016 12:28',	},
			{	id:'6035028163',	noWo:'WO/2016/34667',	noSo:'SO/2016/95165',	noPo:'654041920',	customer:'Melindo Cipta Agung, PT',	specification:'Cutting - Machining - Heat Treatment',	status:'Heat Treatment',	datetime:'13-10-2016 2:13',	},
			{	id:'8360924188',	noWo:'WO/2016/55037',	noSo:'SO/2016/85905',	noPo:'986250863',	customer:'Gabungan Industri Alat-alat Mobil dan Motor (GIAMM)',	specification:'Cutting - Machining - Heat Treatment',	status:'Heat Treatment',	datetime:'13-10-2016 13:15',	},
			{	id:'6335011897',	noWo:'WO/2016/53413',	noSo:'SO/2016/17508',	noPo:'940154297',	customer:'Mutindo Bumi Persada, PT',	specification:'Cutting - Machining - Heat Treatment',	status:'Heat Treatment',	datetime:'13-10-2016 19:49',	},
			{	id:'9128370724',	noWo:'WO/2016/90123',	noSo:'SO/2016/42113',	noPo:'362564045',	customer:'Intertruck Parts, PD',	specification:'Cutting - Machining - Heat Treatment',	status:'Heat Treatment',	datetime:'13-10-2016 16:8',	},
			{	id:'9662528182',	noWo:'WO/2016/98252',	noSo:'SO/2016/27900',	noPo:'899870578',	customer:'Panata Jaya Mandiri, PT',	specification:'Cutting - Machining - Heat Treatment',	status:'Heat Treatment',	datetime:'13-10-2016 14:28',	},
			{	id:'6035659100',	noWo:'WO/2016/85620',	noSo:'SO/2016/90395',	noPo:'339266694',	customer:'Selamat Sempurna Tbk., PT',	specification:'Cutting',	status:'Cutting',	datetime:'13-10-2016 7:42',	},
			{	id:'7954984800',	noWo:'WO/2016/85593',	noSo:'SO/2016/47018',	noPo:'601450084',	customer:'Martena Mccray, PT',	specification:'Cutting',	status:'Cutting',	datetime:'13-10-2016 12:58',	},
		],
		grid: function() {
			var result = [];
			$.each(data.workOrder.raw, function(k, v) {
				var arr = $.map(v, function(el) { return el; });
				result.push(arr);
			});

			return result;
		},
	},


	production: {
		raw: [								
			{	id:'1095545218',	noWo:'WO/2016/30316',	type:'Cutting - Machining - Heat Treatment',	machine:'M0-10',	date:'01/09/2016',	time:'15:00',	},
			{	id:'1485502254',	noWo:'WO/2016/94025',	type:'Cutting - Machining',	machine:'M8-97',	date:'01/09/2016',	time:'15:00',	},
			{	id:'1878364168',	noWo:'WO/2016/57749',	type:'Cutting',	machine:'M1-10',	date:'01/09/2016',	time:'16:00',	},
			{	id:'2267445826',	noWo:'WO/2016/27007',	type:'Cutting - Machining - Heat Treatment',	machine:'M5-75',	date:'01/09/2016',	time:'16:00',	},
			{	id:'2976820013',	noWo:'WO/2016/74301',	type:'Cutting',	machine:'M7-21',	date:'01/09/2016',	time:'15:00',	},
			{	id:'3406830926',	noWo:'WO/2016/36968',	type:'Cutting - Machining',	machine:'M4-26',	date:'01/09/2016',	time:'16:00',	},
			{	id:'3652968870',	noWo:'WO/2016/14834',	type:'Cutting',	machine:'M9-38',	date:'01/09/2016',	time:'11:00',	},
			{	id:'3756993868',	noWo:'WO/2016/82039',	type:'Cutting - Machining - Heat Treatment',	machine:'M7-81',	date:'01/09/2016',	time:'10:00',	},
			{	id:'3860896262',	noWo:'WO/2016/55330',	type:'Cutting - Machining - Heat Treatment',	machine:'M4-16',	date:'01/09/2016',	time:'9:00',	},
			{	id:'4227379122',	noWo:'WO/2016/71680',	type:'Cutting - Machining - Heat Treatment',	machine:'M2-23',	date:'01/09/2016',	time:'12:00',	},
			{	id:'5133105697',	noWo:'WO/2016/12585',	type:'Cutting',	machine:'M5-75',	date:'01/09/2016',	time:'9:00',	},
			{	id:'5651250107',	noWo:'WO/2016/98644',	type:'Cutting - Machining',	machine:'M8-84',	date:'01/09/2016',	time:'14:00',	},
			{	id:'5849300794',	noWo:'WO/2016/22269',	type:'Cutting - Machining - Heat Treatment',	machine:'M9-27',	date:'01/09/2016',	time:'11:00',	},
			{	id:'5870833307',	noWo:'WO/2016/31657',	type:'Cutting',	machine:'M6-34',	date:'01/09/2016',	time:'16:00',	},
			{	id:'5988650190',	noWo:'WO/2016/60188',	type:'Cutting - Machining',	machine:'M0-51',	date:'01/09/2016',	time:'12:00',	},
			{	id:'6020491335',	noWo:'WO/2016/69609',	type:'Cutting - Machining',	machine:'M1-64',	date:'01/09/2016',	time:'16:00',	},
			{	id:'6035028163',	noWo:'WO/2016/34667',	type:'Cutting - Machining - Heat Treatment',	machine:'M1-57',	date:'01/09/2016',	time:'11:00',	},
			{	id:'6035659100',	noWo:'WO/2016/85620',	type:'Cutting',	machine:'M8-27',	date:'01/09/2016',	time:'16:00',	},
			{	id:'6335011897',	noWo:'WO/2016/53413',	type:'Cutting - Machining - Heat Treatment',	machine:'M0-99',	date:'01/09/2016',	time:'10:00',	},
			{	id:'6346667037',	noWo:'WO/2016/42058',	type:'Cutting',	machine:'M6-60',	date:'01/09/2016',	time:'14:00',	},
			{	id:'6449287682',	noWo:'WO/2016/19390',	type:'Cutting - Machining',	machine:'M1-65',	date:'01/09/2016',	time:'12:00',	},
			{	id:'6662486232',	noWo:'WO/2016/77640',	type:'Cutting - Machining - Heat Treatment',	machine:'M8-38',	date:'01/09/2016',	time:'14:00',	},
			{	id:'6790797824',	noWo:'WO/2016/11712',	type:'Cutting - Machining - Heat Treatment',	machine:'M3-62',	date:'01/09/2016',	time:'16:00',	},
			{	id:'7954984800',	noWo:'WO/2016/85593',	type:'Cutting',	machine:'M3-27',	date:'01/09/2016',	time:'16:00',	},
			{	id:'8129734759',	noWo:'WO/2016/37880',	type:'Cutting - Machining - Heat Treatment',	machine:'M5-61',	date:'01/09/2016',	time:'16:00',	},
			{	id:'8339683973',	noWo:'WO/2016/49717',	type:'Cutting - Machining - Heat Treatment',	machine:'M9-14',	date:'01/09/2016',	time:'9:00',	},
			{	id:'8360924188',	noWo:'WO/2016/55037',	type:'Cutting - Machining - Heat Treatment',	machine:'M6-47',	date:'01/09/2016',	time:'12:00',	},
			{	id:'8657251926',	noWo:'WO/2016/19857',	type:'Cutting',	machine:'M0-14',	date:'01/09/2016',	time:'15:00',	},
			{	id:'8788041259',	noWo:'WO/2016/60816',	type:'Cutting - Machining - Heat Treatment',	machine:'M5-90',	date:'01/09/2016',	time:'12:00',	},
			{	id:'8924586113',	noWo:'WO/2016/60719',	type:'Cutting - Machining',	machine:'M6-37',	date:'01/09/2016',	time:'9:00',	},
			{	id:'9030548108',	noWo:'WO/2016/91679',	type:'Cutting - Machining',	machine:'M1-46',	date:'01/09/2016',	time:'15:00',	},
			{	id:'9128370724',	noWo:'WO/2016/90123',	type:'Cutting - Machining - Heat Treatment',	machine:'M7-87',	date:'01/09/2016',	time:'12:00',	},
			{	id:'9635649043',	noWo:'WO/2016/40553',	type:'Cutting - Machining',	machine:'M1-47',	date:'01/09/2016',	time:'14:00',	},
			{	id:'9662528182',	noWo:'WO/2016/98252',	type:'Cutting - Machining - Heat Treatment',	machine:'M2-74',	date:'01/09/2016',	time:'15:00',	},
			{	id:'9784311708',	noWo:'WO/2016/66896',	type:'Cutting - Machining',	machine:'M8-66',	date:'01/09/2016',	time:'16:00',	},
		],
		grid: function() {
			var result = [];
			$.each(data.production.raw, function(k, v) {
				v.machine = data.production.raw[k].machine;
				v.date = data.production.raw[k].date;
				v.time = data.production.raw[k].time;

				var arr = $.map(v, function(el) { return el; });
				result.push(arr);
			});

			return result;
		},
		gridEmpty: function() {
			var result = [];
			var rawEmpty = JSON.parse(JSON.stringify(data.production.raw));
			$.each(rawEmpty, function(k, v) {
				v.machine = '';
				v.date = '';
				v.time = '';

				var arr = $.map(v, function(el) { return el; });
				result.push(arr);
			});

			return result;
		},
	},


	deliveryOrder: {
		raw: [						
			{	id:'9701362634',	noDo:'FDO82555162', noso:'SO/2016/22912',	customer:'PT. Astra Otoparts Tbk.',	address:'Jl. pegangsaan dua, kelapa gading',	date:'2016-11-02', 	dateback:'', status:'On Process',	},
			{	id:'6832634287',	noDo:'FDO19512571', noso:'SO/2016/22326',	customer:'Asno Horie Indonesia, PT',	address:'Kedoya Raya',						date:'2016-11-03', 	dateback:'', status:'On Process',	},
			{	id:'5882623912',	noDo:'FDO88665243', noso:'SO/2016/22355',	customer:'FCC Indonesia, PT',	address:'Jalan Panjang',							date:'2016-11-04', 	dateback:'', status:'On Process',	},
			{	id:'3311259973',	noDo:'FDO27726483', noso:'SO/2016/223765',	customer:'Asianet Spring Indonesia, PT',	address:'Srengseng',					date:'2016-11-05', 	dateback:'', status:'On Process',	},
			{	id:'3564113565',	noDo:'FDO36389264', noso:'SO/2016/22325',	customer:'Nusantara Buana Sakti, PT',	address:'Joglo',							date:'2016-11-06', 	dateback:'', status:'On Process',	},
			{	id:'9089249923',	noDo:'FDO52794662', noso:'SO/2016/22316',	customer:'Topas Multifinance, PT',	address:'Meruya',								date:'2016-11-07', 	dateback:'', status:'On Process',	},
			{	id:'1960366205',	noDo:'FDO51740078', noso:'SO/2016/22314',	customer:'Selamat Sempana Perkasa, PT',	address:'Cengkareng',						date:'2016-11-08', 	dateback:'', status:'On Process',	},
			{	id:'7251905792',	noDo:'FDO73389252', noso:'SO/2016/22312',	customer:'Ichikoh Indonesia, PT',	address:'Grogol',								date:'2016-11-09', 	dateback:'', status:'On Process',	},
			{	id:'9880457860',	noDo:'FDO69822606', noso:'SO/2016/22399',	customer:'IRC Inoac Indonesia, PT [Factory]',	address:'Petamburan',				date:'2016-11-10', 	dateback:'', status:'On Process',	},
			{	id:'9905056203',	noDo:'FDO62142343', noso:'SO/2016/22368',	customer:'Rema Tip Top Indonesia, PT',	address:'Palmerah',							date:'2016-11-11', 	dateback:'', status:'On Process',	},
			{	id:'9805327646',	noDo:'FDO31358693', noso:'SO/2016/22345',	customer:'Fondanusa Aditama, PT',	address:'Kemanggisan',							date:'2016-11-12', 	dateback:'', status:'On Process',	},
			{	id:'8689320487',	noDo:'FDO21188468', noso:'SO/2016/22332',	customer:'Ingress Malindo Ventures, PT',	address:'Slipi',						date:'2016-11-13', 	dateback:'', 		   status:'Delivered',	},
			{	id:'2815460746',	noDo:'FDO53913947', noso:'SO/2016/22322',	customer:'Enam Lima Rubber Industri, PT',	address:'Rawa Buaya',					date:'2016-11-14', 	dateback:'', 		   status:'Delivered',	},
			{	id:'1003715799',	noDo:'FDO92284356', noso:'SO/2016/22390',	customer:'Frina Lestari Nusantara, PT',	address:'Mangga Besar',						date:'2016-11-15', 	dateback:'2016-11-20', status:'Delivered',	},
			{	id:'6491196192',	noDo:'FDO27679308', noso:'SO/2016/22388',	customer:'Prima Sarana Usaha, PT',	address:'Tubagus Angke',						date:'2016-11-16', 	dateback:'2016-11-18', status:'Delivered',	},
			{	id:'7207567808',	noDo:'FDO87583852', noso:'SO/2016/22369',	customer:'Kaneta Indonesia, PT',	address:'Kali Deres',							date:'2016-11-17', 	dateback:'2016-11-19', status:'Delivered',	},
			{	id:'5070824124',	noDo:'FDO89166488', noso:'SO/2016/22377',	customer:'Wijara Nagatsupazki, PT',	address:'Petojo',								date:'2016-11-18', 	dateback:'2016-11-18', status:'Delivered',	},
			{	id:'9158376594',	noDo:'FDO72417848', noso:'SO/2016/22322',	customer:'Mulia Industrindo Tbk., PT',	address:'Sukabumi',							date:'2016-11-19', 	dateback:'2016-11-19', status:'Delivered',	},
			{	id:'5646056027',	noDo:'FDO61605763', noso:'SO/2016/22343',	customer:'Prapat Jaya',	address:'Kampung Baru',										date:'2016-11-20', 	dateback:'2016-11-20', status:'Delivered',	},
			{	id:'2980889132',	noDo:'FDO36581462', noso:'SO/2016/22311',	customer:'Sinar Agung Pemuda, PT',	address:'Jembatan Lima',						date:'2016-11-21', 	dateback:'2016-11-21', status:'Delivered',	},
			{	id:'7814562343',	noDo:'FDO97275453', noso:'SO/2016/22321',	customer:'JRD Bright Motor Assembler, PT',	address:'Kota Bambu',					date:'2016-11-22', 	dateback:'2016-11-22', status:'Delivered',	},
			{	id:'1471409508',	noDo:'FDO72172084', noso:'SO/2016/22389',	customer:'JRD Finance Utama, PT',	address:'Tambora',								date:'2016-11-23', 	dateback:'2016-11-23', status:'Delivered',	},
			{	id:'7336956530',	noDo:'FDO62511410', noso:'SO/2016/22362',	customer:'Herman Interco, PT',	address:'Puri Kembangan',							date:'2016-11-24', 	dateback:'2016-11-24', status:'Delivered',	},
			{	id:'1953279952',	noDo:'FDO20855272', noso:'SO/2016/22378',	customer:'Astra International Tbk., PT',	address:'Bojong',						date:'2016-11-02', 	dateback:'2016-11-02', status:'Delivered',	},
			{	id:'4800247520',	noDo:'FDO53487615', noso:'SO/2016/22399',	customer:'Nachi Indonesia, PT',	address:'Duri Kosambi',								date:'2016-11-03', 	dateback:'2016-11-03', status:'Delivered',	},
			{	id:'2933565021',	noDo:'FDO74757715', noso:'SO/2016/22302',	customer:'Rabana Investindo, PT',	address:'Duri Kepa',							date:'2016-11-04', 	dateback:'2016-11-04', status:'Delivered',	},
			{	id:'6587958936',	noDo:'FDO25927532', noso:'SO/2016/22309',	customer:'Panata Jaya Mandiri, PT [Factory]',	address:'Mangga Dua',				date:'2016-11-05', 	dateback:'2016-11-05', status:'Delivered',	},
			{	id:'5206634909',	noDo:'FDO13313128', noso:'SO/2016/22345',	customer:'Ikatan Ahli Teknik Otomotive (IATO)',	address:'Taman Sari',				date:'2016-11-06', 	dateback:'2016-11-06', status:'Delivered',	},
			{	id:'9916236719',	noDo:'FDO60366889', noso:'SO/2016/22393',	customer:'Melindo Cipta Agung, PT',	address:'Basmoll ',								date:'2016-11-07', 	dateback:'2016-11-07', status:'Delivered',	},
			{	id:'2889837770',	noDo:'FDO88774964', noso:'SO/2016/22366',	customer:'Gabungan Industri Alat-alat Mobil dan Motor (GIAMM)',	address:'Jelambar',	date:'2016-11-08', 	dateback:'2016-11-08', status:'Delivered',	},
			{	id:'6545136956',	noDo:'FDO24701715', noso:'SO/2016/22323',	customer:'Mutindo Bumi Persada, PT',	address:'Rawa Belong',						date:'2016-11-09', 	dateback:'2016-11-09', status:'Delivered',	},
			{	id:'8706995789',	noDo:'FDO38819148', noso:'SO/2016/22344',	customer:'Intertruck Parts, PD',	address:'Batu Sari',							date:'2016-11-10', 	dateback:'2016-11-10', status:'Delivered',	},
			{	id:'7815210498',	noDo:'FDO23811015', noso:'SO/2016/22333',	customer:'Panata Jaya Mandiri, PT',	address:'Bandara Sokarno Hatta ',				date:'2016-11-11', 	dateback:'2016-11-11', status:'Delivered',	},
			{	id:'5957080399',	noDo:'FDO27327810', noso:'SO/2016/22393',	customer:'Selamat Sempurna Tbk., PT',	address:'Tanjung Duren',					date:'2016-11-12', 	dateback:'2016-11-12', status:'Delivered',	},
			{	id:'6783889588',	noDo:'FDO77268842', noso:'SO/2016/22322',	customer:'Martena Mccray, PT',	address:'Tomang',									date:'2016-11-13', 	dateback:'2016-11-13', status:'Delivered',	},
		],						
		grid: function() {
			var result = [];
			$.each(data.deliveryOrder.raw, function(k, v) {
				var arr = $.map(v, function(el) { return el; });
				result.push(arr);
			});

			return result;
		},
	},


	invoice: {
		raw: [								
			{	No:'I0001',	noInvoice:'INV/2016/54065',	soldto:'PT Astra Otoparts Tbk.'      , address:'Jl. pegangsaan dua, kelapa gading'			,npwp:'0227363',	nopesanan:'NP82555162', noDo:'DO/2016/10201:SO/2016/BLN:SEP',	syaratpembayaran:'01/09/2016', statu:'Pending'	},
			{	No:'I0002',	noInvoice:'INV/2016/71516',	soldto:'PT Tobita Club'              , address:'Kedoya Raya'			,npwp:'0738138',	nopesanan:'NP19512571', noDo:'DO/2016/10202:SO/2016/BLN:SEP',	syaratpembayaran:'02/09/2016', statu:'Pending'	},
			{	No:'I0003',	noInvoice:'INV/2016/22086',	soldto:'PT Manganji Group'           , address:'Jalan Panjang'			,npwp:'0913642',	nopesanan:'NP88665243', noDo:'DO/2016/10203:SO/2016/BLN:SEP',	syaratpembayaran:'03/09/2016', statu:'Pending'	},
			{	No:'I0004',	noInvoice:'INV/2016/14163',	soldto:'PT Marume Kurodo Lte'        , address:'Srengseng'				,npwp:'0737772',	nopesanan:'NP27726483', noDo:'DO/2016/10204:SO/2016/BLN:SEP',	syaratpembayaran:'04/09/2016', statu:'Pending'	},
			{	No:'I0005',	noInvoice:'INV/2016/28704',	soldto:'PT Hariken Crush '           , address:'Joglo'					,npwp:'0242870',	nopesanan:'NP36389264', noDo:'DO/2016/10205:SO/2016/BLN:SEP',	syaratpembayaran:'05/09/2016', statu:'Pending'	},
			{	No:'I0006',	noInvoice:'INV/2016/60877',	soldto:'PT Gear Fighter Group'       , address:'Meruya'					,npwp:'0612505',	nopesanan:'NP52794662', noDo:'DO/2016/10206:SO/2016/BLN:SEP',	syaratpembayaran:'06/09/2016', statu:'Pending'	},
			{	No:'I0007',	noInvoice:'INV/2016/86390',	soldto:'PT Mold Jin Kyosuke'         , address:'Cengkareng'				,npwp:'0844813',	nopesanan:'NP51740078', noDo:'DO/2016/10207:SO/2016/BLN:SEP',	syaratpembayaran:'07/09/2016', statu:'Pending'	},
			{	No:'I0008',	noInvoice:'INV/2016/41277',	soldto:'PT Teknologi Dies Jaya'      , address:'Grogol'					,npwp:'0688385',	nopesanan:'NP73389252', noDo:'DO/2016/10208:SO/2016/BLN:SEP',	syaratpembayaran:'08/09/2016', statu:'Pending'	},
			{	No:'I0009',	noInvoice:'INV/2016/32957',	soldto:'PT Griffin Indonesia Jaya'   , address:'Petamburan'				,npwp:'0229854',	nopesanan:'NP69822606', noDo:'DO/2016/10209:SO/2016/BLN:SEP',	syaratpembayaran:'09/09/2016', statu:'Pending'	},
			{	No:'I0010',	noInvoice:'INV/2016/90400',	soldto:'PT Raging Mold Technology'   , address:'Palmerah'				,npwp:'0517633',	nopesanan:'NP62142343', noDo:'DO/2016/10210:SO/2016/BLN:SEP',	syaratpembayaran:'10/09/2016', statu:'Pending'	},
			{	No:'I0011',	noInvoice:'INV/2016/57207',	soldto:'PT Dino Spartan Dies'        , address:'Kemanggisan'			,npwp:'0592599',	nopesanan:'NP31358693', noDo:'DO/2016/10211:SO/2016/BLN:SEP',	syaratpembayaran:'11/09/2016', statu:'Pending'	},
			{	No:'I0012',	noInvoice:'INV/2016/85522',	soldto:'PT Garuda Phoenix Mold dies' , address:'Slipi'					,npwp:'0184643',	nopesanan:'NP21188468', noDo:'DO/2016/10212:SO/2016/BLN:SEP',	syaratpembayaran:'12/09/2016', statu:'Pending'	},
			{	No:'I0013',	noInvoice:'INV/2016/67897',	soldto:'PT Seirin Koko Group'        , address:'Rawa Buaya'				,npwp:'0346846',	nopesanan:'NP53913947', noDo:'DO/2016/10213:SO/2016/BLN:SEP',	syaratpembayaran:'13/09/2016', statu:'Pending'	},
			{	No:'I0014',	noInvoice:'INV/2016/38714',	soldto:'PT Suzuran Koko Group'       , address:'Mangga Besar'			,npwp:'0935163',	nopesanan:'NP92284356', noDo:'DO/2016/10214:SO/2016/BLN:SEP',	syaratpembayaran:'14/09/2016', statu:'Pending'	},
			{	No:'I0015',	noInvoice:'INV/2016/34267',	soldto:'PT Rakuzan Koko Dies'        , address:'Tubagus Angke'			,npwp:'0618915',	nopesanan:'NP27679308', noDo:'DO/2016/10215:SO/2016/BLN:SEP',	syaratpembayaran:'15/09/2016', statu:'Pending'	},
			{	No:'I0016',	noInvoice:'INV/2016/55098',	soldto:'PT Swan Mold Dies Indonesia' , address:'Kali Deres'				,npwp:'0428350',	nopesanan:'NP87583852', noDo:'DO/2016/10216:SO/2016/BLN:SEP',	syaratpembayaran:'16/09/2016', statu:'Pending'	},
			{	No:'I0017',	noInvoice:'INV/2016/97435',	soldto:'PT Genkidama Mold Indonesia' , address:'Petojo'					,npwp:'0155215',	nopesanan:'NP89166488', noDo:'DO/2016/10217:SO/2016/BLN:SEP',	syaratpembayaran:'17/09/2016', statu:'Pending'	},
			{	No:'I0018',	noInvoice:'INV/2016/41246',	soldto:'PT Resinda Mold Dies Group'  , address:'Sukabumi'				,npwp:'0122627',	nopesanan:'NP72417848', noDo:'DO/2016/10218:SO/2016/BLN:SEP',	syaratpembayaran:'18/09/2016', statu:'Pending'	},
			{	No:'I0019',	noInvoice:'INV/2016/53151',	soldto:'PT Rajawali Citra Group'     , address:'Kampung Baru'			,npwp:'0181104',	nopesanan:'NP61605763', noDo:'DO/2016/10219:SO/2016/BLN:SEP',	syaratpembayaran:'19/09/2016', statu:'Pending'	},
			{	No:'I0020',	noInvoice:'INV/2016/85109',	soldto:'PT Suriken Jaya'             , address:'Jembatan Lima'			,npwp:'0284019',	nopesanan:'NP36581462', noDo:'DO/2016/10220:SO/2016/BLN:SEP',	syaratpembayaran:'20/09/2016', statu:'Pending'	},
			{	No:'I0021',	noInvoice:'INV/2016/72348',	soldto:'PT Lukalama Terbuka Kembali' , address:'Kota Bambu'				,npwp:'0911263',	nopesanan:'NP97275453', noDo:'DO/2016/10222:SO/2016/BLN:SEP',	syaratpembayaran:'21/09/2016', statu:'Pending'	},
			{	No:'I0022',	noInvoice:'INV/2016/64758',	soldto:'PT Kirigakure Mold Ltd'      , address:'Tambora'				,npwp:'0650928',	nopesanan:'NP72172084', noDo:'DO/2016/10223:SO/2016/BLN:SEP',	syaratpembayaran:'22/09/2016', statu:'Pending'	},
			{	No:'I0023',	noInvoice:'INV/2016/41864',	soldto:'PT Kazikage Dies Ltd'        , address:'Puri Kembangan'			,npwp:'0420833',	nopesanan:'NP62511410', noDo:'DO/2016/10224:SO/2016/BLN:SEP',	syaratpembayaran:'23/09/2016', statu:'Pending'	},
			{	No:'I0024',	noInvoice:'INV/2016/85725',	soldto:'PT Cakara Elang Ltd'         , address:'Bojong'					,npwp:'0309420',	nopesanan:'NP20855272', noDo:'DO/2016/10225:SO/2016/BLN:SEP',	syaratpembayaran:'24/09/2016', statu:'Pending'	},
			{	No:'I0025',	noInvoice:'INV/2016/94399',	soldto:'PT Falcon Ltd'               , address:'Duri Kosambi'			,npwp:'0313521',	nopesanan:'NP53487615', noDo:'DO/2016/10226:SO/2016/BLN:SEP',	syaratpembayaran:'25/09/2016', statu:'Pending'	},
			{	No:'I0026',	noInvoice:'INV/2016/64912',	soldto:'PT Quota Ltd'                , address:'Duri Kepa'				,npwp:'0808593',	nopesanan:'NP74757715', noDo:'DO/2016/10227:SO/2016/BLN:SEP',	syaratpembayaran:'26/09/2016', statu:'Pending'	},
			{	No:'I0027',	noInvoice:'INV/2016/81287',	soldto:'PT Wilder Mold Steel'        , address:'Mangga Dua'				,npwp:'0984123',	nopesanan:'NP25927532', noDo:'DO/2016/10228:SO/2016/BLN:SEP',	syaratpembayaran:'27/09/2016', statu:'Pending'	},
			{	No:'I0028',	noInvoice:'INV/2016/85115',	soldto:'PT Sari bunga Mold'          , address:'Taman Sari'				,npwp:'0541296',	nopesanan:'NP13313128', noDo:'DO/2016/10229:SO/2016/BLN:SEP',	syaratpembayaran:'28/09/2016', statu:'Pending'	},
			{	No:'I0029',	noInvoice:'INV/2016/69589',	soldto:'PT Cinta Waras'              , address:'Basmoll '				,npwp:'0473033',	nopesanan:'NP60366889', noDo:'DO/2016/10230:SO/2016/BLN:SEP',	syaratpembayaran:'29/09/2016', statu:'Pending'	},
			{	No:'I0030',	noInvoice:'INV/2016/27378',	soldto:'PT Dumbledor Magic Dies'     , address:'Jelambar'				,npwp:'0332206',	nopesanan:'NP88774964', noDo:'DO/2016/10231:SO/2016/BLN:SEP',	syaratpembayaran:'30/09/2016', statu:'Pending'	},
			{	No:'I0031',	noInvoice:'INV/2016/20836',	soldto:'PT Kurcaci Technology'       , address:'Rawa Belong'			,npwp:'0785806',	nopesanan:'NP24701715', noDo:'DO/2016/10232:SO/2016/BLN:SEP',	syaratpembayaran:'31/09/2016', statu:'Pending'	},
			{	No:'I0032',	noInvoice:'INV/2016/59954',	soldto:'PT Kijang Ltd'               , address:'Batu Sari'				,npwp:'0256965',	nopesanan:'NP38819148', noDo:'DO/2016/10233:SO/2016/BLN:SEP',	syaratpembayaran:'01/10/2016', statu:'Pending'	},
			{	No:'I0033',	noInvoice:'INV/2016/97015',	soldto:'PT Phantom Indonesia'        , address:'Bandara Sokarno Hatta '	,npwp:'0961555',	nopesanan:'NP23811015', noDo:'DO/2016/10234:SO/2016/BLN:SEP',	syaratpembayaran:'02/10/2016', statu:'Pending'  },
			{	No:'I0034',	noInvoice:'INV/2016/85738',	soldto:'PT Marino  Yuya Group'       , address:'Tanjung Duren'			,npwp:'0328564',	nopesanan:'NP27327810', noDo:'DO/2016/10235:SO/2016/BLN:SEP',	syaratpembayaran:'03/10/2016', statu:'Pending'  },
			{	No:'I0035',	noInvoice:'INV/2016/88274',	soldto:'PT Marino Kouya Group'       , address:'Tomang'					,npwp:'0206115',	nopesanan:'NP77268842', noDo:'DO/2016/10236:SO/2016/BLN:SEP',	syaratpembayaran:'04/10/2016', statu:'Pending'	},
		],						
		grid: function() {
			var result = [];
			$.each(data.invoice.raw, function(k, v) {
				var arr = $.map(v, function(el) { return el; });
				result.push(arr);
			});

			return result;
		},
	},


	receivable: {
		raw: [								
			{	No:'R0001',  noivoice:'INV/2016/54065',	perusahaan:'PT Astra Otoparts Tbk.'      , amount:'20000000', 	duedate:'10/30/2016',	status:'<p class="infostatus">Paid</p>', paymentdate:'10/29/2016'},
			{	No:'R0002',  noivoice:'INV/2016/54066',	perusahaan:'PT Tobita Club'              , amount:'20000000', 	duedate:'10/29/2016',	status:'<p class="infostatus">Not Paid</p>', paymentdate:''},
			{	No:'R0003',  noivoice:'INV/2016/54067',	perusahaan:'PT Manganji Group'           , amount:'20000000', 	duedate:'10/30/2016',	status:'<p class="infostatus">Not Paid</p>', paymentdate:''},
			{	No:'R0004',  noivoice:'INV/2016/54068',	perusahaan:'PT Marume Kurodo Lte'        , amount:'235000000', 	duedate:'10/22/2016',	status:'<p class="infostatus">Not Paid</p>', paymentdate:''},
			{	No:'R0005',  noivoice:'INV/2016/54069',	perusahaan:'PT Hariken Crush '           , amount:'235000000', 	duedate:'10/11/2016',	status:'<p class="infostatus">Not Paid</p>', paymentdate:''},
			{	No:'R0006',  noivoice:'INV/2016/54060',	perusahaan:'PT Gear Fighter Group'       , amount:'235000000', 	duedate:'10/30/2016',	status:'<p class="infostatus">Not Paid</p>', paymentdate:''},
			{	No:'R0007',  noivoice:'INV/2016/54012',	perusahaan:'PT Mold Jin Kyosuke'         , amount:'235000000', 	duedate:'10/30/2016',	status:'<p class="infostatus">Not Paid</p>', paymentdate:''},
			{	No:'R0008',  noivoice:'INV/2016/54011',	perusahaan:'PT Teknologi Dies Jaya'      , amount:'235000000', 	duedate:'10/11/2016',	status:'<p class="infostatus">Not Paid</p>', paymentdate:''},
			{	No:'R0009',  noivoice:'INV/2016/54013',	perusahaan:'PT Griffin Indonesia Jaya'   , amount:'20000000', 	duedate:'10/30/2016',	status:'<p class="infostatus">Not Paid</p>', paymentdate:''},
			{	No:'R0010',  noivoice:'INV/2016/54015',	perusahaan:'PT Raging Mold Technology'   , amount:'20000000', 	duedate:'10/30/2016',	status:'<p class="infostatus">Not Paid</p>', paymentdate:''},
			{	No:'R0011',  noivoice:'INV/2016/54016',	perusahaan:'PT Dino Spartan Dies'        , amount:'20000000', 	duedate:'10/30/2016',	status:'<p class="infostatus">Not Paid</p>', paymentdate:''},
			{	No:'R0012',  noivoice:'INV/2016/54017',	perusahaan:'PT Garuda Phoenix Mold dies' , amount:'20000000', 	duedate:'10/30/2016',	status:'<p class="infostatus">Not Paid</p>', paymentdate:''},
			{	No:'R0013',  noivoice:'INV/2016/54045',	perusahaan:'PT Seirin Koko Group'        , amount:'20000000', 	duedate:'10/30/2016',	status:'<p class="infostatus">Not Paid</p>', paymentdate:''},
			{	No:'R0014',  noivoice:'INV/2016/54018',	perusahaan:'PT Suzuran Koko Group'       , amount:'20000000', 	duedate:'10/30/2016',	status:'<p class="infostatus">Not Paid</p>', paymentdate:''},
			{	No:'R0015',  noivoice:'INV/2016/54020',	perusahaan:'PT Rakuzan Koko Dies'        , amount:'25000000', 	duedate:'10/30/2016',	status:'<p class="infostatus">Not Paid</p>', paymentdate:''},
			{	No:'R0016',  noivoice:'INV/2016/54019',	perusahaan:'PT Swan Mold Dies Indonesia' , amount:'25000000', 	duedate:'10/30/2016',	status:'<p class="infostatus">Not Paid</p>', paymentdate:''},
			{	No:'R0017',  noivoice:'INV/2016/54021',	perusahaan:'PT Genkidama Mold Indonesia' , amount:'25000000', 	duedate:'10/30/2016',	status:'<p class="infostatus">Not Paid</p>', paymentdate:''},
			{	No:'R0018',  noivoice:'INV/2016/54022',	perusahaan:'PT Resinda Mold Dies Group'  , amount:'25000000', 	duedate:'10/30/2016',	status:'<p class="infostatus">Not Paid</p>', paymentdate:''},
			{	No:'R0019',  noivoice:'INV/2016/54023',	perusahaan:'PT Rajawali Citra Group'     , amount:'25000000', 	duedate:'10/30/2016',	status:'<p class="infostatus">Not Paid</p>', paymentdate:''},
			{	No:'R0020',  noivoice:'INV/2016/54024',	perusahaan:'PT Suriken Jaya'             , amount:'20000000', 	duedate:'10/30/2016',	status:'<p class="infostatus">Not Paid</p>', paymentdate:''},
			{	No:'R0021',  noivoice:'INV/2016/54025',	perusahaan:'PT Lukalama Terbuka Kembali' , amount:'20000000', 	duedate:'10/30/2016',	status:'<p class="infostatus">Not Paid</p>', paymentdate:''},
			{	No:'R0022',  noivoice:'INV/2016/54026',	perusahaan:'PT Kirigakure Mold Ltd'      , amount:'20000000', 	duedate:'10/30/2016',	status:'<p class="infostatus">Not Paid</p>', paymentdate:''},
			{	No:'R0023',  noivoice:'INV/2016/54027',	perusahaan:'PT Kazikage Dies Ltd'        , amount:'20000000', 	duedate:'10/30/2016',	status:'<p class="infostatus">Not Paid</p>', paymentdate:''},
			{	No:'R0024',  noivoice:'INV/2016/54028',	perusahaan:'PT Cakara Elang Ltd'         , amount:'20000000', 	duedate:'10/30/2016',	status:'<p class="infostatus">Not Paid</p>', paymentdate:''},
			{	No:'R0025',  noivoice:'INV/2016/54039',	perusahaan:'PT Falcon Ltd'               , amount:'20000000', 	duedate:'10/30/2016',	status:'<p class="infostatus">Not Paid</p>', paymentdate:''},
			{	No:'R0026',  noivoice:'INV/2016/54030',	perusahaan:'PT Quota Ltd'                , amount:'34500000', 	duedate:'10/30/2016',	status:'<p class="infostatus">Not Paid</p>', paymentdate:''},
			{	No:'R0027',  noivoice:'INV/2016/54031',	perusahaan:'PT Wilder Mold Steel'        , amount:'34500000', 	duedate:'10/30/2016',	status:'<p class="infostatus">Not Paid</p>', paymentdate:''},
			{	No:'R0028',  noivoice:'INV/2016/54032',	perusahaan:'PT Sari bunga Mold'          , amount:'34500000', 	duedate:'10/30/2016',	status:'<p class="infostatus">Not Paid</p>', paymentdate:''},
			{	No:'R0030',  noivoice:'INV/2016/54033',	perusahaan:'PT Dumbledor Magic Dies'     , amount:'34500000', 	duedate:'10/30/2016',	status:'<p class="infostatus">Not Paid</p>', paymentdate:''},
			{	No:'R0029',  noivoice:'INV/2016/54043',	perusahaan:'PT Cinta Waras'              , amount:'34500000', 	duedate:'10/30/2016',	status:'<p class="infostatus">Not Paid</p>', paymentdate:''},
			{	No:'R0031',  noivoice:'INV/2016/54089',	perusahaan:'PT Kurcaci Technology'       , amount:'20000000', 	duedate:'10/30/2016',	status:'<p class="infostatus">Not Paid</p>', paymentdate:''},
			{	No:'R0032',  noivoice:'INV/2016/54066',	perusahaan:'PT Kijang Ltd'               , amount:'20000000', 	duedate:'10/30/2016',	status:'<p class="infostatus">Not Paid</p>', paymentdate:''},
			{	No:'R0033',  noivoice:'INV/2016/54098',	perusahaan:'PT Phantom Indonesia'        , amount:'20000000', 	duedate:'10/30/2016',  	status:'<p class="infostatus">Not Paid</p>', paymentdate:''},
			{	No:'R0034',  noivoice:'INV/2016/54033',	perusahaan:'PT Marino  Yuya Group'       , amount:'20000000', 	duedate:'10/30/2016',  	status:'<p class="infostatus">Not Paid</p>', paymentdate:''},
			{	No:'R0035',  noivoice:'INV/2016/54033',	perusahaan:'PT Marino Kouya Group'       , amount:'20000000', 	duedate:'10/30/2016',	status:'<p class="infostatus">Not Paid</p>', paymentdate:''},
		],						
		grid: function() {
			var result = [];
			$.each(data.receivable.raw, function(k, v) {
				var arr = $.map(v, function(el) { return el; });
				result.push(arr);
			});

			return result;
		},
	},


	purchase: {
		raw: [						
			{	id:'3863006104',	noPo:'POM20162099',	supplier:'Daido Kogyo Co. Ltd.',	status:'Cancel',	date:'09/01/2016',	},
			{	id:'7923295278',	noPo:'POM20166527',	supplier:'Daido Kogyo Co. Ltd.',	status:'Waiting for approval',	date:'10/01/2016',	},
			{	id:'1663555352',	noPo:'POM20166499',	supplier:'Daido Kogyo Co. Ltd.',	status:'Sent',	date:'11/01/2016',	},
			{	id:'4245034923',	noPo:'POM20164423',	supplier:'Daido Kogyo Co. Ltd.',	status:'Sent',	date:'12/01/2016',	},
			{	id:'4813911670',	noPo:'POM20163146',	supplier:'Daido Kogyo Co. Ltd.',	status:'Cancel',	date:'13/01/2016',	},
			{	id:'6879156737',	noPo:'POM20167069',	supplier:'Daido Kogyo Co. Ltd.',	status:'Cancel',	date:'14/01/2016',	},
			{	id:'3943794923',	noPo:'POM20163253',	supplier:'Daido Kogyo Co. Ltd.',	status:'Received',	date:'15/01/2016',	},
			{	id:'9368793301',	noPo:'POM20161991',	supplier:'Daido Kogyo Co. Ltd.',	status:'Sent',	date:'16/01/2016',	},
			{	id:'9963415909',	noPo:'POM20167003',	supplier:'Daido Kogyo Co. Ltd.',	status:'Waiting for approval',	date:'17/01/2016',	},
			{	id:'7795078882',	noPo:'POM20163167',	supplier:'Daido Kogyo Co. Ltd.',	status:'Received',	date:'18/01/2016',	},
			{	id:'1969615410',	noPo:'POM20163988',	supplier:'Daido Kogyo Co. Ltd.',	status:'Sent',	date:'19/01/2016',	},
			{	id:'7067999925',	noPo:'POM20169295',	supplier:'Daido Kogyo Co. Ltd.',	status:'Sent',	date:'20/01/2016',	},
			{	id:'3850540749',	noPo:'POM20166228',	supplier:'Daido Kogyo Co. Ltd.',	status:'Sent',	date:'21/01/2016',	},
			{	id:'4764274154',	noPo:'POM20167287',	supplier:'Daido Kogyo Co. Ltd.',	status:'Received',	date:'22/01/2016',	},
			{	id:'2505478696',	noPo:'POM20168311',	supplier:'Daido Die & Mold Steel Solutions, Co. Ltd.',	status:'Received',	date:'23/01/2016',	},
			{	id:'5888550513',	noPo:'POM20163196',	supplier:'Daido Die & Mold Steel Solutions, Co. Ltd.',	status:'Sent',	date:'24/01/2016',	},
			{	id:'7834966027',	noPo:'POM20162700',	supplier:'Daido Die & Mold Steel Solutions, Co. Ltd.',	status:'Sent',	date:'25/01/2016',	},
			{	id:'4289436922',	noPo:'POM20162996',	supplier:'Daido Die & Mold Steel Solutions, Co. Ltd.',	status:'Waiting for approval',	date:'26/01/2016',	},
			{	id:'5499645372',	noPo:'POM20165164',	supplier:'Daido Die & Mold Steel Solutions, Co. Ltd.',	status:'Waiting for approval',	date:'27/01/2016',	},
			{	id:'4833492221',	noPo:'POM20164040',	supplier:'Daido Die & Mold Steel Solutions, Co. Ltd.',	status:'Waiting for approval',	date:'28/01/2016',	},
			{	id:'9514857118',	noPo:'POM20168705',	supplier:'Daido Die & Mold Steel Solutions, Co. Ltd.',	status:'Received',	date:'29/01/2016',	},
			{	id:'2490671784',	noPo:'POM20168947',	supplier:'Daido Die & Mold Steel Solutions, Co. Ltd.',	status:'Sent',	date:'30/01/2016',	},
			{	id:'4534568424',	noPo:'POM20164531',	supplier:'Daido Die & Mold Steel Solutions, Co. Ltd.',	status:'Sent',	date:'31/01/2016',	},
			{	id:'6215235228',	noPo:'POM20164089',	supplier:'Daido Die & Mold Steel Solutions, Co. Ltd.',	status:'Sent',	date:'01/02/2016',	},
			{	id:'5239760079',	noPo:'POM20169758',	supplier:'Daido Die & Mold Steel Solutions, Co. Ltd.',	status:'Sent',	date:'02/02/2016',	},
			{	id:'6493974472',	noPo:'POM20162168',	supplier:'Daido Die & Mold Steel Solutions, Co. Ltd.',	status:'Sent',	date:'03/02/2016',	},
			{	id:'9552608927',	noPo:'POM20162630',	supplier:'Daido Die & Mold Steel Solutions, Co. Ltd.',	status:'Sent',	date:'04/02/2016',	},
			{	id:'3087983445',	noPo:'POM20168735',	supplier:'Daido Die & Mold Steel Solutions, Co. Ltd.',	status:'Waiting for approval',	date:'05/02/2016',	},
			{	id:'2993284178',	noPo:'POM20161816',	supplier:'Daido Die & Mold Steel Solutions, Co. Ltd.',	status:'Waiting for approval',	date:'06/02/2016',	},
		],						
		grid: function() {
			var result = [];
			$.each(data.purchase.raw, function(k, v) {
				var arr = $.map(v, function(el) { return el; });
				result.push(arr);
			});

			return result;
		},
	},


	inventory: {
		raw: [						
			{	product:'DC11-F',   serial:'DC11-F00320960',	location:'ST01C3',	thickness:'25', width:'35', diameter:'0', length:'37', unit:'1',  kg:'200',	},
			{	product:'DC53-F',   serial:'DC53-F00320960',	location:'ST01C3',	thickness:'25', width:'35', diameter:'0', length:'37', unit:'10',  kg:'700',	},
			{	product:'DHA1-F',   serial:'DHA1-F00320960',	location:'ST01C3',	thickness:'25', width:'35', diameter:'0', length:'37', unit:'13',  kg:'500',	},
			{	product:'GOA-F',    serial:'GOA-F00320960',		location:'ST01C3',	thickness:'25', width:'35', diameter:'0', length:'37', unit:'16',  kg:'400',	},
			{	product:'DC11-R2',  serial:'DC11-R200320960',	location:'ST01C3',	thickness:'0', width:'0', diameter:'23', length:'37',  unit:'21',  kg:'300',	},
			{	product:'DC53-R1',  serial:'DC53-R100320960',	location:'ST01C3',	thickness:'0', width:'0', diameter:'23', length:'37',  unit:'1',  kg:'200',	},
			{	product:'DHA1-R2',  serial:'DHA1-R200320960',	location:'ST01C3',	thickness:'0', width:'0', diameter:'23', length:'37',  unit:'1',  kg:'200',	},
			{	product:'GOA-R2', 	serial:'GOA-R200320960',	location:'ST01C3',	thickness:'0', width:'0', diameter:'23', length:'37',  unit:'1',  kg:'200',	},
			{	product:'DC11-R2',  serial:'DC11-R200320960',	location:'ST01C3',	thickness:'0', width:'0', diameter:'23', length:'37',  unit:'1',  kg:'200',	},
			{	product:'DC53-R1',  serial:'DC53-R100320960',	location:'ST01C3',	thickness:'0', width:'0', diameter:'23', length:'37',  unit:'1',  kg:'200',	},
			{	product:'DHA1-R2',  serial:'DHA1-R200320960',	location:'ST01C3',	thickness:'0', width:'0', diameter:'23', length:'37',  unit:'1',  kg:'200',	},
			{	product:'GOA-R2', 	serial:'GOA-R200320960',	location:'ST01C3',	thickness:'0', width:'0', diameter:'23', length:'37',  unit:'1',  kg:'200',	},
		],						
		grid: function() {
			var result = [];
			$.each(data.inventory.raw, function(k, v) {
				var arr = $.map(v, function(el) { return el; });
				result.push(arr);
			});

			return result;
		},
	},


	warehouse: {
		raw: [						
			{	id:'4762988348',	name:'KNN-783',	type:'Baja Tipis',	quantity:'114',	status:'Warning',	location:'R54L23',	},
			{	id:'4331053117',	name:'SFG-870',	type:'Baja Tipis',	quantity:'962',	status:'OK',	location:'R50L56',	},
			{	id:'2541136093',	name:'SKB-271',	type:'Baja Tipis',	quantity:'883',	status:'OK',	location:'R96L64',	},
			{	id:'6395067054',	name:'ZKV-243',	type:'Baja Tipis',	quantity:'947',	status:'OK',	location:'R69L96',	},
			{	id:'1103590149',	name:'TNJ-740',	type:'Baja Tipis',	quantity:'181',	status:'OK',	location:'R63L76',	},
			{	id:'4824636979',	name:'FBW-317',	type:'Baja Tipis',	quantity:'329',	status:'OK',	location:'R88L77',	},
			{	id:'9370269501',	name:'AON-325',	type:'Baja Tipis',	quantity:'166',	status:'OK',	location:'R48L63',	},
			{	id:'7612912130',	name:'PSS-86',	type:'Baja Tebal',	quantity:'1113',	status:'Shipped',	location:'R25L80',	},
			{	id:'9174520901',	name:'LBI-815',	type:'Baja Tebal',	quantity:'535',	status:'Shipped',	location:'R68L56',	},
			{	id:'9534154252',	name:'YNY-787',	type:'Baja Lempengan',	quantity:'534',	status:'OK',	location:'R89L22',	},
			{	id:'8003482461',	name:'CDX-204',	type:'Baja Lempengan',	quantity:'149',	status:'OK',	location:'R28L88',	},
			{	id:'7056036302',	name:'OZC-448',	type:'Baja Lempengan',	quantity:'116',	status:'OK',	location:'R61L36',	},
			{	id:'6196645300',	name:'CIV-17',	type:'Baja Lempengan',	quantity:'672',	status:'OK',	location:'R14L32',	},
			{	id:'6277940770',	name:'XFT-568',	type:'Baja Lempengan',	quantity:'97',	status:'Warning',	location:'R49L98',	},
			{	id:'8649190196',	name:'NHT-943',	type:'Baja Lempengan',	quantity:'659',	status:'OK',	location:'R66L45',	},
			{	id:'5314882603',	name:'VBG-99',	type:'Baja Lempengan',	quantity:'395',	status:'OK',	location:'R42L56',	},
			{	id:'5485256020',	name:'MBP-966',	type:'Baja Lempengan',	quantity:'893',	status:'OK',	location:'R73L83',	},
			{	id:'8555662466',	name:'GJI-682',	type:'Baja Lempengan',	quantity:'809',	status:'OK',	location:'R22L86',	},
			{	id:'1108056243',	name:'ZLM-633',	type:'Baja Lempengan',	quantity:'236',	status:'OK',	location:'R90L49',	},
			{	id:'7521801941',	name:'PRY-75',	type:'Baja Lempengan',	quantity:'571',	status:'OK',	location:'R33L84',	},
			{	id:'3185601578',	name:'JUO-998',	type:'Baja Silinder',	quantity:'885',	status:'OK',	location:'R95L80',	},
			{	id:'1436002329',	name:'NPZ-247',	type:'Baja Silinder',	quantity:'615',	status:'OK',	location:'R93L65',	},
			{	id:'1354304514',	name:'SKN-258',	type:'Baja Silinder',	quantity:'0',	status:'Out of stock',	location:'R49L98',	},
			{	id:'8347748843',	name:'VYD-889',	type:'Baja Silinder',	quantity:'867',	status:'OK',	location:'R23L15',	},
			{	id:'3649494381',	name:'GFR-178',	type:'Baja Silinder',	quantity:'853',	status:'OK',	location:'R42L77',	},
			{	id:'1395851078',	name:'UWP-16',	type:'Baja Silinder',	quantity:'803',	status:'OK',	location:'R86L12',	},
			{	id:'7826426300',	name:'VFR-95',	type:'Baja Silinder',	quantity:'0',	status:'Out of stock',	location:'R53L31',	},
			{	id:'7678548973',	name:'ELM-261',	type:'Baja Silinder',	quantity:'586',	status:'OK',	location:'R12L92',	},
			{	id:'4190474412',	name:'NJU-730',	type:'Baja Silinder',	quantity:'457',	status:'OK',	location:'R79L42',	},
			{	id:'9461983403',	name:'QWR-729',	type:'Baja Silinder',	quantity:'737',	status:'OK',	location:'R84L17',	},
			{	id:'3490423441',	name:'XHO-951',	type:'Baja Silinder',	quantity:'975',	status:'OK',	location:'R12L10',	},
			{	id:'6457593064',	name:'GCT-610',	type:'Baja Silinder',	quantity:'71',	status:'Warning',	location:'R67L26',	},
			{	id:'8077830419',	name:'LWI-490',	type:'Baja Silinder',	quantity:'794',	status:'OK',	location:'R31L13',	},
			{	id:'5028464844',	name:'QAZ-375',	type:'Baja Silinder',	quantity:'396',	status:'OK',	location:'R27L56',	},
			{	id:'8672872375',	name:'NNS-838',	type:'Baja Silinder',	quantity:'292',	status:'OK',	location:'R76L43',	},
			{	id:'1536897412',	name:'IWR-222',	type:'Baja Silinder',	quantity:'1000',	status:'OK',	location:'R20L41',	},
			{	id:'7634752793',	name:'BTH-871',	type:'Baja Silinder',	quantity:'434',	status:'OK',	location:'R97L99',	},
			{	id:'9865797615',	name:'PPT-239',	type:'Baja Silinder',	quantity:'648',	status:'OK',	location:'R10L65',	},
			{	id:'9657858513',	name:'GOM-447',	type:'Baja Silinder',	quantity:'865',	status:'OK',	location:'R92L45',	},
		],						
		grid: function() {
			var result = [];
			$.each(data.warehouse.raw, function(k, v) {
				var arr = $.map(v, function(el) { return el; });
				result.push(arr);
			});

			return result;
		},
	},

	adddeliveryorder: {
		raw: [					
			{	nowo:'WO/2016/30316', product:'DC11-F 10 x 100 x 200',	thickness:'13',	width:'100',	diameter:'0',	length:'200',	pcs:'5',	kg:'90',	Serial:'DC11-F,DC11-F00320960,ST01C3,25,35,0,37,1',		status:'Ready to Deliver',	},
			{	nowo:'WO/2016/30317', product:'DC53-F 15 x 200 x 300',	thickness:'15',	width:'200',	diameter:'0',	length:'300',	pcs:'7', 	kg:'150',	Serial:'DC53-F,DC53-F00320960,ST01C3,25,35,0,37,1',		status:'Ready to Deliver',	},
			{	nowo:'WO/2016/30318', product:'DC11-R dia 46 x 200',	thickness:'0',	width:'0',		diameter:'46',	length:'200',	pcs:'5', 	kg:'100',	Serial:'DC11-R2,DC11-R200320960,ST01C3,0,0,23,37,1',	status:'Ready to Deliver',	},
			
		],								
		grid: function() {
			var result = [];
			$.each(data.adddeliveryorder.raw, function(k, v) {
				var arr = $.map(v, function(el) { return el; });
				result.push(arr);
			});

			return result;
		}
	},

	addinvoice: {
		raw: [					
			{ no:'1',	description:'DC11-F 13 x 100 x 200 = 5 Pcs',	spesification:'Cutting - Machining',					nodo:'FDO82555162', 	pcs:'5',	kg:'90',	unitprice:'180000',	discount:'10',	subtotal:'14.580.000',	},
			{ no:'2',	description:'DC53-F 15 x 200 x 300 = 7 Pcs',	spesification:'Cutting - Machining - Heat Treatment',	nodo:'FDO82555162', 	pcs:'7',	kg:'150',	unitprice:'230000', discount:'20',	subtotal:'27.600.000',	},
			{ no:'3',	description:'DC11-R dia 46 x 200 = 5 Pcs',		spesification:'Cutting',								nodo:'FDO82555162', 	pcs:'5',	kg:'100',	unitprice:'190000', discount:'12',	subtotal:'16.720.000',	},
			
		],								
		grid: function() {
			var result = [];
			$.each(data.addinvoice.raw, function(k, v) {
				var arr = $.map(v, function(el) { return el; });
				result.push(arr);
			});

			return result;
		}
	},

	proposeorder: {
		raw: [						
			{	No:'1',   supplier:'Daido Kogyo CO.,LTD',				date:'November 2016',	pcs:'25',  kg:'50000', status:'Not Approve', },
			{	No:'2',   supplier:'Daido Die Mold Solutions CO.,LTD',	date:'November 2016',	pcs:'25',  kg:'50000', status:'Approve', },
			{	No:'3',   supplier:'Daido Kogyo CO.,LTD',				date:'Oktober 2016',	pcs:'25',  kg:'50000', status:'Approve', },
			{	No:'4',   supplier:'Daido Die Mold Solutions CO.,LTD',	date:'Oktober 2016',	pcs:'25',  kg:'50000', status:'Approve', },
		],						
		grid: function() {
			var result = [];
			$.each(data.proposeorder.raw, function(k, v) {
				var arr = $.map(v, function(el) { return el; });
				result.push(arr);
			});

			return result;
		},
	},

	addproposeorder: {
		raw: [					
			{	material:'DC11',	thickness:'20',	width:'405', diameter:'0',	pcs:'5',	kg:'90',	},
			{	material:'DC53',	thickness:'10',	width:'305', diameter:'0',	pcs:'7',	kg:'150',	},
			{	material:'GOA',		thickness:'0',	width:'0',	 diameter:'30',	pcs:'5',	kg:'100',	},
			
		],								
		grid: function() {
			var result = [];
			$.each(data.addproposeorder.raw, function(k, v) {
				var arr = $.map(v, function(el) { return el; });
				result.push(arr);
			});

			return result;
		}
	},

	addpurchaseorder: {
		raw: [		
			{	material:'DC11',	thickness:'20',	width:'405', diameter:'0',	pcs:'5',	kg:'90',	price:'100',	amount:'9000',	},
			{	material:'DC53',	thickness:'10',	width:'305', diameter:'0',	pcs:'7',	kg:'150',	price:'200',	amount:'30000',	},
			{	material:'GOA',		thickness:'0',	width:'0',	 diameter:'30',	pcs:'5',	kg:'100',	price:'300',	amount:'30000',	},
		],						
		grid: function() {
			var result = [];
			$.each(data.addpurchaseorder.raw, function(k, v) {
				var arr = $.map(v, function(el) { return el; });
				result.push(arr);
			});

			return result;
		},
	},

	newitem: {
		raw: [						
			{	noinvoice:'INV/2016/54065', Supplier:'Daido Kogyo Co. Ltd.',  					   bldate:'10/01/2016', top:'30',	invoice:'<span class="glyphicon glyphicon-save"></span> Download', packinglist:'<span class="glyphicon glyphicon-save"></span> Download',  billoflading:'<span class="glyphicon glyphicon-save"></span> Download', 	},
			{	noinvoice:'INV/2016/54066', Supplier:'Daido Kogyo Co. Ltd.',  					   bldate:'10/02/2016', top:'30',	invoice:'<span class="glyphicon glyphicon-save"></span> Download', packinglist:'<span class="glyphicon glyphicon-save"></span> Download',  billoflading:'<span class="glyphicon glyphicon-save"></span> Download', 	},
			{	noinvoice:'INV/2016/54012', Supplier:'Daido Kogyo Co. Ltd.',  					   bldate:'10/03/2016', top:'30',	invoice:'<span class="glyphicon glyphicon-save"></span> Download', packinglist:'<span class="glyphicon glyphicon-save"></span> Download',  billoflading:'<span class="glyphicon glyphicon-save"></span> Download', 	},
			{	noinvoice:'INV/2016/54011', Supplier:'Daido Kogyo Co. Ltd.',  					   bldate:'10/04/2016', top:'30',	invoice:'<span class="glyphicon glyphicon-save"></span> Download', packinglist:'<span class="glyphicon glyphicon-save"></span> Download',  billoflading:'<span class="glyphicon glyphicon-save"></span> Download', 	},
			{	noinvoice:'INV/2016/54013', Supplier:'Daido Kogyo Co. Ltd.',  					   bldate:'10/05/2016', top:'30',	invoice:'<span class="glyphicon glyphicon-save"></span> Download', packinglist:'<span class="glyphicon glyphicon-save"></span> Download',  billoflading:'<span class="glyphicon glyphicon-save"></span> Download', 	},
			{	noinvoice:'INV/2016/54014', Supplier:'Daido Kogyo Co. Ltd.',  					   bldate:'10/06/2016', top:'30',	invoice:'<span class="glyphicon glyphicon-save"></span> Download', packinglist:'<span class="glyphicon glyphicon-save"></span> Download',  billoflading:'<span class="glyphicon glyphicon-save"></span> Download', 	},
			{	noinvoice:'INV/2016/54015', Supplier:'Daido Kogyo Co. Ltd.',  					   bldate:'10/07/2016', top:'30',	invoice:'<span class="glyphicon glyphicon-save"></span> Download', packinglist:'<span class="glyphicon glyphicon-save"></span> Download',  billoflading:'<span class="glyphicon glyphicon-save"></span> Download', 	},
			{	noinvoice:'INV/2016/54018', Supplier:'Daido Kogyo Co. Ltd.',  					   bldate:'10/08/2016', top:'30',	invoice:'<span class="glyphicon glyphicon-save"></span> Download', packinglist:'<span class="glyphicon glyphicon-save"></span> Download',  billoflading:'<span class="glyphicon glyphicon-save"></span> Download', 	},
			{	noinvoice:'INV/2016/54019', Supplier:'Daido Kogyo Co. Ltd.',  					   bldate:'10/09/2016', top:'30',	invoice:'<span class="glyphicon glyphicon-save"></span> Download', packinglist:'<span class="glyphicon glyphicon-save"></span> Download',  billoflading:'<span class="glyphicon glyphicon-save"></span> Download', 	},
			{	noinvoice:'INV/2016/54020', Supplier:'Daido Kogyo Co. Ltd.',  					   bldate:'10/10/2016', top:'30',	invoice:'<span class="glyphicon glyphicon-save"></span> Download', packinglist:'<span class="glyphicon glyphicon-save"></span> Download',  billoflading:'<span class="glyphicon glyphicon-save"></span> Download', 	},
			{	noinvoice:'INV/2016/54021', Supplier:'Daido Kogyo Co. Ltd.',  					   bldate:'10/11/2016', top:'30',	invoice:'<span class="glyphicon glyphicon-save"></span> Download', packinglist:'<span class="glyphicon glyphicon-save"></span> Download',  billoflading:'<span class="glyphicon glyphicon-save"></span> Download', 	},
			{	noinvoice:'INV/2016/54022', Supplier:'Daido Kogyo Co. Ltd.',  					   bldate:'10/13/2016', top:'30',	invoice:'<span class="glyphicon glyphicon-save"></span> Download', packinglist:'<span class="glyphicon glyphicon-save"></span> Download',  billoflading:'<span class="glyphicon glyphicon-save"></span> Download', 	},
			{	noinvoice:'INV/2016/54023', Supplier:'Daido Kogyo Co. Ltd.',  					   bldate:'10/12/2016', top:'30',	invoice:'<span class="glyphicon glyphicon-save"></span> Download', packinglist:'<span class="glyphicon glyphicon-save"></span> Download',  billoflading:'<span class="glyphicon glyphicon-save"></span> Download', 	},
			{	noinvoice:'INV/2016/54024', Supplier:'Daido Kogyo Co. Ltd.',  					   bldate:'10/14/2016', top:'30',	invoice:'<span class="glyphicon glyphicon-save"></span> Download', packinglist:'<span class="glyphicon glyphicon-save"></span> Download',  billoflading:'<span class="glyphicon glyphicon-save"></span> Download', 	},
			{	noinvoice:'INV/2016/54025', Supplier:'Daido Kogyo Co. Ltd.',  					   bldate:'10/15/2016', top:'30',	invoice:'<span class="glyphicon glyphicon-save"></span> Download', packinglist:'<span class="glyphicon glyphicon-save"></span> Download',  billoflading:'<span class="glyphicon glyphicon-save"></span> Download', 	},
			{	noinvoice:'INV/2016/54026', Supplier:'Daido Kogyo Co. Ltd.',  					   bldate:'10/16/2016', top:'30',	invoice:'<span class="glyphicon glyphicon-save"></span> Download', packinglist:'<span class="glyphicon glyphicon-save"></span> Download',  billoflading:'<span class="glyphicon glyphicon-save"></span> Download', 	},
			{	noinvoice:'INV/2016/54027', Supplier:'Daido Kogyo Co. Ltd.',  					   bldate:'10/17/2016', top:'30',	invoice:'<span class="glyphicon glyphicon-save"></span> Download', packinglist:'<span class="glyphicon glyphicon-save"></span> Download',  billoflading:'<span class="glyphicon glyphicon-save"></span> Download', 	},
			{	noinvoice:'INV/2016/54028', Supplier:'Daido Kogyo Co. Ltd.',  					   bldate:'10/18/2016', top:'30',	invoice:'<span class="glyphicon glyphicon-save"></span> Download', packinglist:'<span class="glyphicon glyphicon-save"></span> Download',  billoflading:'<span class="glyphicon glyphicon-save"></span> Download', 	},
			{	noinvoice:'INV/2016/54029', Supplier:'Daido Kogyo Co. Ltd.',  					   bldate:'10/19/2016', top:'30',	invoice:'<span class="glyphicon glyphicon-save"></span> Download', packinglist:'<span class="glyphicon glyphicon-save"></span> Download',  billoflading:'<span class="glyphicon glyphicon-save"></span> Download', 	},
			{	noinvoice:'INV/2016/54030', Supplier:'Daido Die & Mold Steel Solutions, Co. Ltd.', bldate:'10/20/2016', top:'30',	invoice:'<span class="glyphicon glyphicon-save"></span> Download', packinglist:'<span class="glyphicon glyphicon-save"></span> Download',  billoflading:'<span class="glyphicon glyphicon-save"></span> Download', 	},
			{	noinvoice:'INV/2016/54089', Supplier:'Daido Die & Mold Steel Solutions, Co. Ltd.', bldate:'10/21/2016', top:'30',	invoice:'<span class="glyphicon glyphicon-save"></span> Download', packinglist:'<span class="glyphicon glyphicon-save"></span> Download',  billoflading:'<span class="glyphicon glyphicon-save"></span> Download', 	},
			{	noinvoice:'INV/2016/54072', Supplier:'Daido Die & Mold Steel Solutions, Co. Ltd.', bldate:'10/22/2016', top:'30',	invoice:'<span class="glyphicon glyphicon-save"></span> Download', packinglist:'<span class="glyphicon glyphicon-save"></span> Download',  billoflading:'<span class="glyphicon glyphicon-save"></span> Download', 	},
			{	noinvoice:'INV/2016/54221', Supplier:'Daido Die & Mold Steel Solutions, Co. Ltd.', bldate:'10/23/2016', top:'30',	invoice:'<span class="glyphicon glyphicon-save"></span> Download', packinglist:'<span class="glyphicon glyphicon-save"></span> Download',  billoflading:'<span class="glyphicon glyphicon-save"></span> Download', 	},
			{	noinvoice:'INV/2016/54309', Supplier:'Daido Die & Mold Steel Solutions, Co. Ltd.', bldate:'10/24/2016', top:'30',	invoice:'<span class="glyphicon glyphicon-save"></span> Download', packinglist:'<span class="glyphicon glyphicon-save"></span> Download',  billoflading:'<span class="glyphicon glyphicon-save"></span> Download', 	},
			{	noinvoice:'INV/2016/21333', Supplier:'Daido Die & Mold Steel Solutions, Co. Ltd.', bldate:'10/25/2016', top:'30',	invoice:'<span class="glyphicon glyphicon-save"></span> Download', packinglist:'<span class="glyphicon glyphicon-save"></span> Download',  billoflading:'<span class="glyphicon glyphicon-save"></span> Download', 	},
			{	noinvoice:'INV/2016/12817', Supplier:'Daido Die & Mold Steel Solutions, Co. Ltd.', bldate:'10/28/2016', top:'30',	invoice:'<span class="glyphicon glyphicon-save"></span> Download', packinglist:'<span class="glyphicon glyphicon-save"></span> Download',  billoflading:'<span class="glyphicon glyphicon-save"></span> Download', 	},
			
		],						
		grid: function() {
			var result = [];
			$.each(data.newitem.raw, function(k, v) {
				var arr = $.map(v, function(el) { return el; });
				result.push(arr);
			});

			return result;
		},
	},

	payment: {
		raw: [						
			{	noinvoice:'INV/2016/54065', Supplier:'Daido Kogyo Co. Ltd.',  					   bldate:'10/01/2016', top:'30',	invoice:'<span class="glyphicon glyphicon-save"></span> Download', packinglist:'<span class="glyphicon glyphicon-save"></span> Download',  billoflading:'<span class="glyphicon glyphicon-save"></span> Download',  status:'<p class="infostatus">Not Paid</p>',	},
			{	noinvoice:'INV/2016/54066', Supplier:'Daido Kogyo Co. Ltd.',  					   bldate:'10/02/2016', top:'30',	invoice:'<span class="glyphicon glyphicon-save"></span> Download', packinglist:'<span class="glyphicon glyphicon-save"></span> Download',  billoflading:'<span class="glyphicon glyphicon-save"></span> Download',  status:'<p> Paid</p>',	},
			{	noinvoice:'INV/2016/54012', Supplier:'Daido Kogyo Co. Ltd.',  					   bldate:'10/03/2016', top:'30',	invoice:'<span class="glyphicon glyphicon-save"></span> Download', packinglist:'<span class="glyphicon glyphicon-save"></span> Download',  billoflading:'<span class="glyphicon glyphicon-save"></span> Download',  status:'<p> Paid</p>',	},
			{	noinvoice:'INV/2016/54011', Supplier:'Daido Kogyo Co. Ltd.',  					   bldate:'10/04/2016', top:'30',	invoice:'<span class="glyphicon glyphicon-save"></span> Download', packinglist:'<span class="glyphicon glyphicon-save"></span> Download',  billoflading:'<span class="glyphicon glyphicon-save"></span> Download',  status:'<p>Not Paid</p>',	},
			{	noinvoice:'INV/2016/54013', Supplier:'Daido Kogyo Co. Ltd.',  					   bldate:'10/05/2016', top:'30',	invoice:'<span class="glyphicon glyphicon-save"></span> Download', packinglist:'<span class="glyphicon glyphicon-save"></span> Download',  billoflading:'<span class="glyphicon glyphicon-save"></span> Download',  status:'<p> Paid</p>',	},
			{	noinvoice:'INV/2016/54014', Supplier:'Daido Kogyo Co. Ltd.',  					   bldate:'10/06/2016', top:'30',	invoice:'<span class="glyphicon glyphicon-save"></span> Download', packinglist:'<span class="glyphicon glyphicon-save"></span> Download',  billoflading:'<span class="glyphicon glyphicon-save"></span> Download',  status:'<p> Paid</p>',	},
			{	noinvoice:'INV/2016/54015', Supplier:'Daido Kogyo Co. Ltd.',  					   bldate:'10/07/2016', top:'30',	invoice:'<span class="glyphicon glyphicon-save"></span> Download', packinglist:'<span class="glyphicon glyphicon-save"></span> Download',  billoflading:'<span class="glyphicon glyphicon-save"></span> Download',  status:'<p> Paid</p>',	},
			{	noinvoice:'INV/2016/54018', Supplier:'Daido Kogyo Co. Ltd.',  					   bldate:'10/08/2016', top:'30',	invoice:'<span class="glyphicon glyphicon-save"></span> Download', packinglist:'<span class="glyphicon glyphicon-save"></span> Download',  billoflading:'<span class="glyphicon glyphicon-save"></span> Download',  status:'<p> Paid</p>',	},
			{	noinvoice:'INV/2016/54019', Supplier:'Daido Kogyo Co. Ltd.',  					   bldate:'10/09/2016', top:'30',	invoice:'<span class="glyphicon glyphicon-save"></span> Download', packinglist:'<span class="glyphicon glyphicon-save"></span> Download',  billoflading:'<span class="glyphicon glyphicon-save"></span> Download',  status:'<p> Paid</p>',	},
			{	noinvoice:'INV/2016/54020', Supplier:'Daido Kogyo Co. Ltd.',  					   bldate:'10/10/2016', top:'30',	invoice:'<span class="glyphicon glyphicon-save"></span> Download', packinglist:'<span class="glyphicon glyphicon-save"></span> Download',  billoflading:'<span class="glyphicon glyphicon-save"></span> Download',  status:'<p> Paid</p>',	},
			{	noinvoice:'INV/2016/54021', Supplier:'Daido Kogyo Co. Ltd.',  					   bldate:'10/11/2016', top:'30',	invoice:'<span class="glyphicon glyphicon-save"></span> Download', packinglist:'<span class="glyphicon glyphicon-save"></span> Download',  billoflading:'<span class="glyphicon glyphicon-save"></span> Download',  status:'<p> Paid</p>',	},
			{	noinvoice:'INV/2016/54022', Supplier:'Daido Kogyo Co. Ltd.',  					   bldate:'10/13/2016', top:'30',	invoice:'<span class="glyphicon glyphicon-save"></span> Download', packinglist:'<span class="glyphicon glyphicon-save"></span> Download',  billoflading:'<span class="glyphicon glyphicon-save"></span> Download',  status:'<p> Paid</p>',	},
			{	noinvoice:'INV/2016/54023', Supplier:'Daido Kogyo Co. Ltd.',  					   bldate:'10/12/2016', top:'30',	invoice:'<span class="glyphicon glyphicon-save"></span> Download', packinglist:'<span class="glyphicon glyphicon-save"></span> Download',  billoflading:'<span class="glyphicon glyphicon-save"></span> Download',  status:'<p> Paid</p>',	},
			{	noinvoice:'INV/2016/54024', Supplier:'Daido Kogyo Co. Ltd.',  					   bldate:'10/14/2016', top:'30',	invoice:'<span class="glyphicon glyphicon-save"></span> Download', packinglist:'<span class="glyphicon glyphicon-save"></span> Download',  billoflading:'<span class="glyphicon glyphicon-save"></span> Download',  status:'<p> Paid</p>',	},
			{	noinvoice:'INV/2016/54025', Supplier:'Daido Kogyo Co. Ltd.',  					   bldate:'10/15/2016', top:'30',	invoice:'<span class="glyphicon glyphicon-save"></span> Download', packinglist:'<span class="glyphicon glyphicon-save"></span> Download',  billoflading:'<span class="glyphicon glyphicon-save"></span> Download',  status:'<p> Paid</p>',	},
			{	noinvoice:'INV/2016/54026', Supplier:'Daido Kogyo Co. Ltd.',  					   bldate:'10/16/2016', top:'30',	invoice:'<span class="glyphicon glyphicon-save"></span> Download', packinglist:'<span class="glyphicon glyphicon-save"></span> Download',  billoflading:'<span class="glyphicon glyphicon-save"></span> Download',  status:'<p>Not Paid</p>',	},
			{	noinvoice:'INV/2016/54027', Supplier:'Daido Kogyo Co. Ltd.',  					   bldate:'10/17/2016', top:'30',	invoice:'<span class="glyphicon glyphicon-save"></span> Download', packinglist:'<span class="glyphicon glyphicon-save"></span> Download',  billoflading:'<span class="glyphicon glyphicon-save"></span> Download',  status:'<p> Paid</p>',	},
			{	noinvoice:'INV/2016/54028', Supplier:'Daido Kogyo Co. Ltd.',  					   bldate:'10/18/2016', top:'30',	invoice:'<span class="glyphicon glyphicon-save"></span> Download', packinglist:'<span class="glyphicon glyphicon-save"></span> Download',  billoflading:'<span class="glyphicon glyphicon-save"></span> Download',  status:'<p> Paid</p>',	},
			{	noinvoice:'INV/2016/54029', Supplier:'Daido Kogyo Co. Ltd.',  					   bldate:'10/19/2016', top:'30',	invoice:'<span class="glyphicon glyphicon-save"></span> Download', packinglist:'<span class="glyphicon glyphicon-save"></span> Download',  billoflading:'<span class="glyphicon glyphicon-save"></span> Download',  status:'<p> Paid</p>',	},
			{	noinvoice:'INV/2016/54030', Supplier:'Daido Die & Mold Steel Solutions, Co. Ltd.', bldate:'10/20/2016', top:'30',	invoice:'<span class="glyphicon glyphicon-save"></span> Download', packinglist:'<span class="glyphicon glyphicon-save"></span> Download',  billoflading:'<span class="glyphicon glyphicon-save"></span> Download',  status:'<p> Paid</p>',	},
			{	noinvoice:'INV/2016/54089', Supplier:'Daido Die & Mold Steel Solutions, Co. Ltd.', bldate:'10/21/2016', top:'30',	invoice:'<span class="glyphicon glyphicon-save"></span> Download', packinglist:'<span class="glyphicon glyphicon-save"></span> Download',  billoflading:'<span class="glyphicon glyphicon-save"></span> Download',  status:'<p> Paid</p>',	},
			{	noinvoice:'INV/2016/54072', Supplier:'Daido Die & Mold Steel Solutions, Co. Ltd.', bldate:'10/22/2016', top:'30',	invoice:'<span class="glyphicon glyphicon-save"></span> Download', packinglist:'<span class="glyphicon glyphicon-save"></span> Download',  billoflading:'<span class="glyphicon glyphicon-save"></span> Download',  status:'<p> Paid</p>',	},
			{	noinvoice:'INV/2016/54221', Supplier:'Daido Die & Mold Steel Solutions, Co. Ltd.', bldate:'10/23/2016', top:'30',	invoice:'<span class="glyphicon glyphicon-save"></span> Download', packinglist:'<span class="glyphicon glyphicon-save"></span> Download',  billoflading:'<span class="glyphicon glyphicon-save"></span> Download',  status:'<p> Paid</p>',	},
			{	noinvoice:'INV/2016/54309', Supplier:'Daido Die & Mold Steel Solutions, Co. Ltd.', bldate:'10/24/2016', top:'30',	invoice:'<span class="glyphicon glyphicon-save"></span> Download', packinglist:'<span class="glyphicon glyphicon-save"></span> Download',  billoflading:'<span class="glyphicon glyphicon-save"></span> Download',  status:'<p> Paid</p>',	},
			{	noinvoice:'INV/2016/21333', Supplier:'Daido Die & Mold Steel Solutions, Co. Ltd.', bldate:'10/25/2016', top:'30',	invoice:'<span class="glyphicon glyphicon-save"></span> Download', packinglist:'<span class="glyphicon glyphicon-save"></span> Download',  billoflading:'<span class="glyphicon glyphicon-save"></span> Download',  status:'<p> Paid</p>',	},
			{	noinvoice:'INV/2016/12817', Supplier:'Daido Die & Mold Steel Solutions, Co. Ltd.', bldate:'10/28/2016', top:'30',	invoice:'<span class="glyphicon glyphicon-save"></span> Download', packinglist:'<span class="glyphicon glyphicon-save"></span> Download',  billoflading:'<span class="glyphicon glyphicon-save"></span> Download',  status:'<p> Paid</p>',	},
		],						
		grid: function() {
			var result = [];
			$.each(data.payment.raw, function(k, v) {
				var arr = $.map(v, function(el) { return el; });
				result.push(arr);
			});

			return result;
		},
	},

	goodreceive: {
		raw: [						
			{	noinvoice:'INV/2016/54065', Supplier:'Daido Kogyo Co. Ltd.',  					   packinglist:'<span class="glyphicon glyphicon-save"></span> Download',  billoflading:'<span class="glyphicon glyphicon-save"></span> Download',  status:'<p class="infostatusreceive">Ready to Receive</p>',		nogr:'',	},
			{	noinvoice:'INV/2016/54066', Supplier:'Daido Kogyo Co. Ltd.',  					   packinglist:'<span class="glyphicon glyphicon-save"></span> Download',  billoflading:'<span class="glyphicon glyphicon-save"></span> Download',  status:'<p> Received</p>',										nogr:'19299212',	},
			{	noinvoice:'INV/2016/54012', Supplier:'Daido Kogyo Co. Ltd.',  					   packinglist:'<span class="glyphicon glyphicon-save"></span> Download',  billoflading:'<span class="glyphicon glyphicon-save"></span> Download',  status:'<p> Received</p>',										nogr:'19299234',	},
			{	noinvoice:'INV/2016/54011', Supplier:'Daido Kogyo Co. Ltd.',  					   packinglist:'<span class="glyphicon glyphicon-save"></span> Download',  billoflading:'<span class="glyphicon glyphicon-save"></span> Download',  status:'<p>Ready to Receive</p>',								nogr:'',	},
			{	noinvoice:'INV/2016/54013', Supplier:'Daido Kogyo Co. Ltd.',  					   packinglist:'<span class="glyphicon glyphicon-save"></span> Download',  billoflading:'<span class="glyphicon glyphicon-save"></span> Download',  status:'<p> Received</p>',										nogr:'19299245',	},
			{	noinvoice:'INV/2016/54014', Supplier:'Daido Kogyo Co. Ltd.',  					   packinglist:'<span class="glyphicon glyphicon-save"></span> Download',  billoflading:'<span class="glyphicon glyphicon-save"></span> Download',  status:'<p> Received</p>',										nogr:'19299266',	},
			{	noinvoice:'INV/2016/54015', Supplier:'Daido Kogyo Co. Ltd.',  					   packinglist:'<span class="glyphicon glyphicon-save"></span> Download',  billoflading:'<span class="glyphicon glyphicon-save"></span> Download',  status:'<p> Received</p>',										nogr:'19299292',	},
			{	noinvoice:'INV/2016/54018', Supplier:'Daido Kogyo Co. Ltd.',  					   packinglist:'<span class="glyphicon glyphicon-save"></span> Download',  billoflading:'<span class="glyphicon glyphicon-save"></span> Download',  status:'<p> Received</p>',										nogr:'19299282',	},
			{	noinvoice:'INV/2016/54019', Supplier:'Daido Kogyo Co. Ltd.',  					   packinglist:'<span class="glyphicon glyphicon-save"></span> Download',  billoflading:'<span class="glyphicon glyphicon-save"></span> Download',  status:'<p> Received</p>',										nogr:'19299234',	},
			{	noinvoice:'INV/2016/54020', Supplier:'Daido Kogyo Co. Ltd.',  					   packinglist:'<span class="glyphicon glyphicon-save"></span> Download',  billoflading:'<span class="glyphicon glyphicon-save"></span> Download',  status:'<p> Received</p>',										nogr:'192992656',	},
			{	noinvoice:'INV/2016/54021', Supplier:'Daido Kogyo Co. Ltd.',  					   packinglist:'<span class="glyphicon glyphicon-save"></span> Download',  billoflading:'<span class="glyphicon glyphicon-save"></span> Download',  status:'<p> Received</p>',										nogr:'192992214',	},
			{	noinvoice:'INV/2016/54022', Supplier:'Daido Kogyo Co. Ltd.',  					   packinglist:'<span class="glyphicon glyphicon-save"></span> Download',  billoflading:'<span class="glyphicon glyphicon-save"></span> Download',  status:'<p> Received</p>',										nogr:'192992921',	},
			{	noinvoice:'INV/2016/54023', Supplier:'Daido Kogyo Co. Ltd.',  					   packinglist:'<span class="glyphicon glyphicon-save"></span> Download',  billoflading:'<span class="glyphicon glyphicon-save"></span> Download',  status:'<p> Received</p>',										nogr:'19292312',	},
			{	noinvoice:'INV/2016/54024', Supplier:'Daido Kogyo Co. Ltd.',  					   packinglist:'<span class="glyphicon glyphicon-save"></span> Download',  billoflading:'<span class="glyphicon glyphicon-save"></span> Download',  status:'<p> Received</p>',										nogr:'19294443',	},
			{	noinvoice:'INV/2016/54025', Supplier:'Daido Kogyo Co. Ltd.',  					   packinglist:'<span class="glyphicon glyphicon-save"></span> Download',  billoflading:'<span class="glyphicon glyphicon-save"></span> Download',  status:'<p> Received</p>',										nogr:'19299211',	},
			{	noinvoice:'INV/2016/54026', Supplier:'Daido Kogyo Co. Ltd.',  					   packinglist:'<span class="glyphicon glyphicon-save"></span> Download',  billoflading:'<span class="glyphicon glyphicon-save"></span> Download',  status:'<p>Ready to Receive</p>',								nogr:'',	},
			{	noinvoice:'INV/2016/54027', Supplier:'Daido Kogyo Co. Ltd.',  					   packinglist:'<span class="glyphicon glyphicon-save"></span> Download',  billoflading:'<span class="glyphicon glyphicon-save"></span> Download',  status:'<p> Received</p>',										nogr:'19299221',	},
			{	noinvoice:'INV/2016/54028', Supplier:'Daido Kogyo Co. Ltd.',  					   packinglist:'<span class="glyphicon glyphicon-save"></span> Download',  billoflading:'<span class="glyphicon glyphicon-save"></span> Download',  status:'<p> Received</p>',										nogr:'19299221',	},
			{	noinvoice:'INV/2016/54029', Supplier:'Daido Kogyo Co. Ltd.',  					   packinglist:'<span class="glyphicon glyphicon-save"></span> Download',  billoflading:'<span class="glyphicon glyphicon-save"></span> Download',  status:'<p> Received</p>',										nogr:'19299221',	},
			{	noinvoice:'INV/2016/54030', Supplier:'Daido Die & Mold Steel Solutions, Co. Ltd.', packinglist:'<span class="glyphicon glyphicon-save"></span> Download',  billoflading:'<span class="glyphicon glyphicon-save"></span> Download',  status:'<p> Received</p>',										nogr:'19299221',	},
			{	noinvoice:'INV/2016/54089', Supplier:'Daido Die & Mold Steel Solutions, Co. Ltd.', packinglist:'<span class="glyphicon glyphicon-save"></span> Download',  billoflading:'<span class="glyphicon glyphicon-save"></span> Download',  status:'<p> Received</p>',										nogr:'19299221',	},
			{	noinvoice:'INV/2016/54072', Supplier:'Daido Die & Mold Steel Solutions, Co. Ltd.', packinglist:'<span class="glyphicon glyphicon-save"></span> Download',  billoflading:'<span class="glyphicon glyphicon-save"></span> Download',  status:'<p> Received</p>',										nogr:'19299221',	},
			{	noinvoice:'INV/2016/54221', Supplier:'Daido Die & Mold Steel Solutions, Co. Ltd.', packinglist:'<span class="glyphicon glyphicon-save"></span> Download',  billoflading:'<span class="glyphicon glyphicon-save"></span> Download',  status:'<p> Received</p>',										nogr:'19299221',	},
			{	noinvoice:'INV/2016/54309', Supplier:'Daido Die & Mold Steel Solutions, Co. Ltd.', packinglist:'<span class="glyphicon glyphicon-save"></span> Download',  billoflading:'<span class="glyphicon glyphicon-save"></span> Download',  status:'<p> Received</p>',										nogr:'19299221',	},
			{	noinvoice:'INV/2016/21333', Supplier:'Daido Die & Mold Steel Solutions, Co. Ltd.', packinglist:'<span class="glyphicon glyphicon-save"></span> Download',  billoflading:'<span class="glyphicon glyphicon-save"></span> Download',  status:'<p> Received</p>',										nogr:'19299221',	},
			{	noinvoice:'INV/2016/12817', Supplier:'Daido Die & Mold Steel Solutions, Co. Ltd.', packinglist:'<span class="glyphicon glyphicon-save"></span> Download',  billoflading:'<span class="glyphicon glyphicon-save"></span> Download',  status:'<p> Received</p>',										nogr:'19299221',	},
		],						
		grid: function() {
			var result = [];
			$.each(data.goodreceive.raw, function(k, v) {
				var arr = $.map(v, function(el) { return el; });
				result.push(arr);
			});

			return result;
		},
	},

	addgoodreceive: {
		raw: [						
			{	no:'1', 	material:'DC11-F',   purchaseorder:'POM20166499',	thickness:'25', width:'35', diameter:'0', 	length:'37', pcs:'90',	kg:'200', },
			{	no:'2', 	material:'DC53-F',   purchaseorder:'POM20166423',	thickness:'25', width:'35', diameter:'0', 	length:'37', pcs:'100',	kg:'300', },
			{	no:'3', 	material:'DHA1-F',   purchaseorder:'POM20166239',	thickness:'25', width:'35', diameter:'0', 	length:'37', pcs:'120',	kg:'400', },
			{	no:'4', 	material:'GOA-F',    purchaseorder:'POM20166245',	thickness:'25', width:'35', diameter:'0', 	length:'37', pcs:'90',	kg:'200', },
			{	no:'5', 	material:'DC11-R2',  purchaseorder:'POM20234528',	thickness:'0', 	width:'0', 	diameter:'23', 	length:'37', pcs:'100',	kg:'300', },
			{	no:'6', 	material:'DC53-R1',  purchaseorder:'POM82293349',	thickness:'0', 	width:'0', 	diameter:'23', 	length:'37', pcs:'120',	kg:'400', },
			{	no:'7', 	material:'DHA1-R2',  purchaseorder:'POM20166239',	thickness:'0', 	width:'0', 	diameter:'23', 	length:'37', pcs:'90',	kg:'200', },
			{	no:'8', 	material:'GOA-R2', 	 purchaseorder:'POM20167839',	thickness:'0', 	width:'0', 	diameter:'23', 	length:'37', pcs:'100',	kg:'300', },
			{	no:'9', 	material:'DC11-R2',  purchaseorder:'POM20168783',	thickness:'0', 	width:'0', 	diameter:'23', 	length:'37', pcs:'120',	kg:'400', },
			{	no:'10', 	material:'DC53-R1',  purchaseorder:'POM29183348',	thickness:'0', 	width:'0', 	diameter:'23', 	length:'37', pcs:'90',	kg:'200', },
			{	no:'11', 	material:'DHA1-R2',  purchaseorder:'POM22384944',	thickness:'0', 	width:'0', 	diameter:'23', 	length:'37', pcs:'100',	kg:'300', },
			{	no:'12', 	material:'GOA-R2', 	 purchaseorder:'POM78333395',	thickness:'0', 	width:'0', 	diameter:'23', 	length:'37', pcs:'120',	kg:'400', },
		],						
		grid: function() {
			var result = [];
			$.each(data.addgoodreceive.raw, function(k, v) {
				var arr = $.map(v, function(el) { return el; });
				result.push(arr);
			});

			return result;
		},
	},

};
