var pages = {

	customerCard: {
		manage: {
			load: function() {
				$('#content').load('pages/customer-card/manage.html', function() {
					listOfValues.loadAll();
				});
			},
		},
		approval: {
			load: function() {
				$('#content').load('pages/customer-card/approval.html', function() {
					listOfValues.loadAll();
				});
			},
		},
		add: {
			load: function() {
				$('#content').load('pages/customer-card/add.html', function() {
					listOfValues.loadAll();
				});
			},
		}
	},


	quotation: {
		manage: {
			load: function() {
				$('#content').load('pages/quotation/manage.html', function() {
					listOfValues.loadAll();
				});
			},
		},
		add: {
			load: function() {
				$('#content').load('pages/quotation/add.html', function() {
					listOfValues.loadAll();
				});
			},
		},
		machining: {
			load: function() {
				$('#content-sub').append('<div id="machiningContent"></div>');
				$.get('pages/quotation/add.machining.html', function(data){
					$('#machiningContent').html(data);
					listOfValues.loadAll();
				});
			},
			unload: function() {
				$('#machiningContent').html('');
			},
		},
		heat: {
			load: function() {
				$('#content-sub').append('<div id="heatContent"></div>');
				$.get('pages/quotation/add.harden.html', function(data){
					$('#heatContent').html(data);
					listOfValues.loadAll();
				});
			},
			unload: function() {
				$('#heatContent').html('');
			},
		},
	},


	salesOrder: {
		manage: {
			load: function() {
				$('#content').load('pages/sales-order/manage.html', function() {
					listOfValues.loadAll();
				});
			},
		},
		inquiry: {
			load: function() {
				$('#content').load('pages/sales-order/inquiry.html', function() {
					listOfValues.loadAll();
				});
			},
		},
		edit: {
			load: function() {
				$('#content').load('pages/sales-order/edit.html', function() {
					listOfValues.loadAll();
					$('.lov-customer').val('10001');
					$('.lov-shop').val('ADASI');
					$('.lov-pricelist').val('Pricelist Public (IDR)');
					listOfValues.customer.name.change();
				});
			},
		},
	},


	workOrder: {
		manage: {
			load: function() {
				$('#content').load('pages/work-order/manage.html', function() {
					listOfValues.loadAll();
				});
			},
		},
		add: {
			load: function() {
				$('#content').load('pages/work-order/add.html', function() {
					listOfValues.worktype.load();
					listOfValues.loadAll();
					
				});
			},
			cutting: {
				load: function(index, values) {
					$('#subcontent' + index).append('<div id="contentCutting"></div>');
					$.get('pages/work-order/add.cutting.html', function(data){
						$('#subcontent' + index + ' #contentCutting').html(data);

						var t = values.thickness + 5;
						var w = values.width + 5;
						var l = values.length + 5;
						var tolerance = t + ' x ' + w + ' x ' + l;

						$('#subcontent'+index+' #contentCutting .alert-info').html('Finish cutting: <a class="alert-link" href="#">'+tolerance+'</a>.');
						$('#subcontent'+index+' #contentCutting .cutting .material').val(values.material);

					});
				},
				unload: function() {
					$('#contentCutting').html('');
				},
			},
			machining: {
				load: function(index, values) {
					$('#subcontent' + index).append('<div id="contentMachining"></div>');
					$.get('pages/work-order/add.machining.html', function(data) {
						$('#subcontent' + index + ' #contentMachining').html(data);

						console.log('values: ', values);
						
						var t = values.thickness + 5;
						var w = values.width + 5;
						var l = values.length + 5;

						$('#subcontent'+index+' #contentMachining .raw-material .material').val(values.material);
						$('#subcontent'+index+' #contentMachining .raw-material .thickness').val(t);
						$('#subcontent'+index+' #contentMachining .raw-material .width').val(w);
						$('#subcontent'+index+' #contentMachining .raw-material .length').val(l);

						$('#subcontent'+index+' #contentMachining .finished-material .material').val(values.material);
						$('#subcontent'+index+' #contentMachining .finished-material .thickness').val(values.thickness);
						$('#subcontent'+index+' #contentMachining .finished-material .width').val(values.width);
						$('#subcontent'+index+' #contentMachining .finished-material .length').val(values.length);
					});
				},
				unload: function() {
					$('#contentMachining').html('');
				},
			},
			harden: {
				load: function(index, values) {
					$('#subcontent' + index).append('<div id="contentHeat"></div>');
					$.get('pages/work-order/add.harden.html', function(data) {
						$('#subcontent' + index + ' #contentHeat').html(data);
						// listOfValues.loadAll();
					});
				},
				unload: function() {
					$('#contentHeat').html('');
				},
			},
		},
		updateStatus: {
			load: function() {
				$('#content').load('pages/work-order/update-status.html', function() {
					listOfValues.loadAll();
				});
			},
		},
		print: {
			load: function() {
				$('#content').load('pages/work-order/print.html', function() {
					listOfValues.loadAll();
				});
			},
		},
	},


	production: {
		manage: {
			load: function() {
				$('#content').load('pages/production/manage.html', function() {
					listOfValues.loadAll();
				});
			},
		},
		add: {
			load: function() {
				$('#content').load('pages/production/add.html', function() {
					listOfValues.loadAll();
				});
			},
		},
		edit: {
			load: function() {
				$('#content').load('pages/production/edit.html', function() {
					listOfValues.loadAll();
				});
			},
		},
	},


	procurement: {
		manage: {
			load: function() {
				$('#content').load('pages/procurement/manage.html', function() {
					listOfValues.loadAll();
				});
			},
		},
		upload: {
			load: function() {
				$('#content').load('pages/procurement/upload.html', function() {
					listOfValues.loadAll();
				});
			},
		},
	},


	purchaseOrder: {
		manage: {
			load: function() {
				$('#content').load('pages/purchase-order/manage.html', function() {
					listOfValues.loadAll();
				});
			},
		},
		add: {
			load: function() {
				$('#content').load('pages/purchase-order/add.html', function() {
					listOfValues.loadAll();
				});
			},
		},
		createpo: {
			load: function() {
				$('#content').load('pages/purchase-order/createpo.html', function() {
					listOfValues.loadAll();
				});
			},
		},
	},


	deliveryOrder: {
		manage: {
			load: function() {
				$('#content').load('pages/delivery-order/manage.html', function() {
					listOfValues.loadAll();
				});
			},
		},
		add: {
			load: function() {
				$('#content').load('pages/delivery-order/add.html', function() {
					listOfValues.loadAll();
				});
			},
		},
	},


	account: {
		invoice: {
			manage: {
				load: function() {
					$('#content').load('pages/invoice/manage.html', function() {
						listOfValues.loadAll();
					});
				},
			},
			add: {
				load: function() {
					$('#content').load('pages/invoice/add.html', function() {
						listOfValues.loadAll();
					});
				},
			},
		},
		receivable: {
			manage: {
				load: function() {
					$('#content').load('pages/account-receivable/manage.html', function() {
						listOfValues.loadAll();
					});
				},
			},
			add:{
				load: function() {
					$('#content').load('pages/account-receivable/add.html', function() {
						listOfValues.loadAll();
					});
				},
			},
		},
	},


	inventory: {
		manage: {
			load: function() {
				$('#content').load('pages/inventory/manage.html', function() {
					listOfValues.loadAll();
				});
			},
		},
		add: {
			load: function() {
				$('#content').load('pages/inventory/add.html', function() {
					listOfValues.loadAll();
				});
			},
		},
		checkStock: {
			load: function() {
				$('#content').load('pages/inventory/check-stock.html', function() {
					listOfValues.loadAll();
				});
			},
		},
	},


	warehouse: {
		manage: {
			load: function() {
				$('#content').load('pages/warehouse/manage.html', function() {
					listOfValues.loadAll();
				});
			},
		},
		add: {
			load: function() {
				$('#content').load('pages/warehouse/add.html', function() {
					listOfValues.loadAll();
				});
			},
		},
	},

	payment: {
		manage: {
			load: function() {
				$('#content').load('pages/payment/manage.html', function() {
					listOfValues.loadAll();
				});
			},
		},
		add: {
			load: function() {
				$('#content').load('pages/payment/add.html', function() {
					listOfValues.loadAll();
				});
			},
		},
	},

	proposeorder: {
		manage: {
			load: function() {
				$('#content').load('pages/propose-order/manage.html', function() {
					listOfValues.loadAll();
				});
			},
		},
		add: {
			load: function() {
				$('#content').load('pages/propose-order/add.html', function() {
					listOfValues.loadAll();
				});
			},
		},
	},

	incomingshipment: {
		manage: {
			load: function() {
				$('#content').load('pages/incomingshipment/manage.html', function() {
					listOfValues.loadAll();
				});
			},
		},
		add: {
			load: function() {
				$('#content').load('pages/incomingshipment/add.html', function() {
					listOfValues.loadAll();
				});
			},
		},
	},

	newitem: {
		manage: {
			load: function() {
				$('#content').load('pages/newitem/manage.html', function() {
					listOfValues.loadAll();
				});
			},
		},
		add: {
			load: function() {
				$('#content').load('pages/newitem/add.html', function() {
					listOfValues.loadAll();
				});
			},
		},
	},

	goodreceive: {
		manage: {
			load: function() {
				$('#content').load('pages/goodreceive/manage.html', function() {
					listOfValues.loadAll();
				});
			},
		},
		add: {
			load: function() {
				$('#content').load('pages/goodreceive/add.html', function() {
					listOfValues.loadAll();
				});
			},
		},
	},

};
