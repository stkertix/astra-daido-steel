var util = {

	toast: {
		success: function(message) {
			toastr.options.timeOut = 7000;
			toastr.success(message, 'Success');
		},
		info: function(message) {
			toastr.options.timeOut = 7000;
			toastr.info(message, 'Information');
		},
		warning: function(message) {
			toastr.options.timeOut = 7000;
			toastr.warning(message, 'Warning');
		},
		error: function(message) {
			toastr.options.timeOut = 7000;
			toastr.error(message, 'Error');
		},
	},


	XLSXInterface: {
		translateTitle : function(data,translator){
			var newData = [];
			for(var i=0; i<data.length; i++){
				newData.push({});
				for(fld in data[i]){
					newData[i][translator[fld]] = data[i][fld];
				}
			}
			return newData;
		},
		loadToJson : function(fileHandler,cb){
			var f = new FileReader();
			f.onload = function(e){
				var data = e.target.result;
				var workbook = XLSX.read(data, {type:"binary"});
				var jsonXLS = XLSX.utils.sheet_to_json(workbook.Sheets[workbook.SheetNames[0]]);
				cb(jsonXLS);
			}
			var str = f.readAsBinaryString(fileHandler.target.files[0]);
		},
		writeToXLSX : function(json, fileName, multiSheet){
			function constructSheetFromJson(json){
				function datenum(v, date1904) {
					if(date1904) v+=1462;
					var epoch = Date.parse(v);
					return (epoch - new Date(Date.UTC(1899, 11, 30))) / (24 * 60 * 60 * 1000);
				};
				function createCell(val){
					var cell = {v: val};
					if(cell.v != null){
						if(typeof cell.v === 'number') cell.t = 'n';
						else if(typeof cell.v === 'boolean') cell.t = 'b';
						else if(cell.v instanceof Date) {
							cell.t = 'n'; cell.z = XLSX.SSF._table[14];
							cell.v = datenum(cell.v);
						}
						else cell.t = 's';                    
					}
					return cell;
				};
				var ws = {};
				var colMax = 0;
				for(var R = 0; R < json.length; R++){
					var C = 0;
					for(var fld in json[R]){
						if(colMax<C) colMax = C;
						if(R==0){
							var cell = createCell(fld);
							var cell_ref = XLSX.utils.encode_cell({c:C,r:R});
							ws[cell_ref] = cell;
						}
						var cell = createCell(json[R][fld]);                    
						var cell_ref = XLSX.utils.encode_cell({c:C,r:R+1});
						ws[cell_ref] = cell;
						C++;
					}
				};
				var range = {s: {c:0, r:0 }, e: {c:colMax, r:json.length}};
				ws['!ref'] = XLSX.utils.encode_range(range);
				return ws;
			};
			function s2ab(s) {
				var buf = new ArrayBuffer(s.length);
				var view = new Uint8Array(buf);
				for (var i=0; i!=s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
					return buf;
			};
			if(multiSheet!=undefined && multiSheet.length>0){
				var wb = {
					SheetNames : multiSheet,
					Sheets : {}
				};
				for(i in multiSheet){
					wb.Sheets[multiSheet[i]] = constructSheetFromJson(json[i]);
				}
			} else {
				var ws = constructSheetFromJson(json);
				var wb = {
					SheetNames : [fileName],
					Sheets : {}
				}; 
				wb.Sheets[fileName] = ws;                
			}
			var wbout = XLSX.write(wb, {bookType:'xlsx', bookSST:true, type: 'binary'});
			saveAs(new Blob([s2ab(wbout)],{type:"application/octet-stream"}), fileName+".xlsx");
		}
	},


	table: {
		action: function(table) {
			$('#editable tbody').on( 'click', '.approve', function () {
				var data = table.row( $(this).parents('tr') ).data();
				$(this).removeClass('btn-default').addClass('btn-primary').html('<i class="fa fa-check"></i>');
				util.toast.success('Data has been approved!');
			} );

			$('#editable tbody').on( 'click', '.updatestatus', function () {
				var data = table.row( $(this).parents('tr') ).data();
				$('.infostatus').replaceWith( "Lunas" );
				$(this).removeClass('btn-default').addClass('btn-info').html('<i class="fa fa-check"></i>');
				util.toast.success('Lunas');
			} );

			$('#editable tbody').on( 'click', '.edit', function () {
				var data = table.row( $(this).parents('tr') ).data();
				// util.toast.success('');
			} );

			$('#editable tbody').on( 'click', '.delete', function () {
				swal({
					title: "Are you sure?",
					text: "You will not be able to recover this data!",
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#DD6B55",
					confirmButtonText: "Yes, delete it!",
					closeOnConfirm: false
				}, function () {
					swal("Deleted!", "Your data has been deleted.", "success");
				});
			} );

			$('#editable tbody').on( 'click', '.status', function () {
				var data = table.row( $(this).parents('tr') ).data();
				$(this).removeClass('btn-default').addClass('btn-primary').html('<i class="fa fa-check"></i>');
				util.toast.success('Job has been finished!');
			} );

			$('#editable tbody').on( 'click', '.updatestatus', function () {
				var data = table.row( $(this).parents('tr') ).data();
				$('.infostatus').replaceWith( "Paid" );
				$(this).removeClass('btn-default').addClass('btn-info').html('<i class="fa fa-check"></i>');
				util.toast.success('Not Paid');
			} );

			$('#editable tbody').on( 'click', '.updatestatusreceive', function () {
				var data = table.row( $(this).parents('tr') ).data();
				$('.infostatusreceive').replaceWith( "Received" );
				$(this).removeClass('btn-default').addClass('btn-info').html('<i class="fa fa-check"></i>');
				util.toast.success('Received');
			} );

		}
	},

};
