var listOfValues = {

	loadAll: function() {
		listOfValues.areas.load();
		listOfValues.areaBarang.load();
		listOfValues.material.load();
		listOfValues.bank.load();
		listOfValues.quote.load();
		listOfValues.yesno.load();
		listOfValues.shop.load();
		listOfValues.pricelist.load();
		listOfValues.status.load();
		listOfValues.customer.name.load();
		listOfValues.salesOrder.noSo.load();
		listOfValues.workOrder.load();
		listOfValues.machine.load();
		listOfValues.cutting.load();
		listOfValues.machining.load();
		listOfValues.harden.load();
		listOfValues.supplier.load();
	},


	areas: {
		data: ['', 'DKI Jakarta', 'Bogor', 'Bekasi', 'Tangerang', 'Bandung'],
		load: function() {
			$.each(listOfValues.areas.data, function(k, v) {
				$(".lov-areas").append($('<option>').text(v).attr('value', v));
			});
		},
	},


	areaBarang: {
		data: ['', 'CL01', 'CL02', 'CL03', 'CL04', 'CL05'],
		load: function() {
			$.each(listOfValues.areaBarang.data, function(k, v) {
				$(".lov-area-barang").append($('<option>').text(v).attr('value', v));
			});
		},
	},


	material: {
		data: ['', 'DC11-F', 'DC11-R', 'DC53-F'],
		load: function() {
			$.each(listOfValues.material.data, function(k, v) {
				// $(".lov-material").empty();
				$(".lov-material").append($('<option>').text(v).attr('value', v));
			});
		},
	},


	machine: {
		data: ['', 'CM01', 'CM02', 'CM03', 'CM04', 'CM05'],
		load: function() {
			$.each(listOfValues.machine.data, function(k, v) {
				$(".lov-machine").append($('<option>').text(v).attr('value', v));
			});
		}
	},

	cutting: {
		data: ['', 'CM01', 'CM02', 'CM03', 'CM04', 'CM05'],
		load: function() {
			$.each(listOfValues.cutting.data, function(k, v) {
				$(".lov-machine-cutting").append($('<option>').text(v).attr('value', v));
			});
		},
	},
	machining: {
		data: ['', 'MM01', 'MM02', 'MM03', 'MM04', 'MM05'],
		load: function() {
			$.each(listOfValues.machining.data, function(k, v) {
				$(".lov-machine-machining").append($('<option>').text(v).attr('value', v));
			});
		},
	},
	harden: {
		data: ['', 'HT01', 'HT02', 'HT03', 'HT04', 'HT05'],
		load: function() {
			$.each(listOfValues.harden.data, function(k, v) {
				$(".lov-machine-harden").append($('<option>').text(v).attr('value', v));
			});
		},
	},


	bank: {
		data: ['BCA', 'BNI', 'BRI', 'Mandiri', 'Permata', 'Mizuho'],
		load: function() {
			$.each(listOfValues.bank.data, function(k, v) {
				$(".lov-bank").append($('<option>').text(v).attr('value', v));
			});
		},
	},

	quote: {
		data: ['', 'SPP', 'SPO', 'LPO'],
		load: function() {
			$.each(listOfValues.quote.data, function(k, v) {
				$(".lov-quote").append($('<option>').text(v).attr('value', v));
			});
		},
	},

	yesno: {
		data: ['', 'Yes', 'No'],
		load: function() {
			$.each(listOfValues.yesno.data, function(k, v) {
				$(".lov-yesno").append($('<option>').text(v).attr('value', v));
			});
		},
	},

	shop: {
		data: ['', 'ADASI'],
		load: function() {
			$.each(listOfValues.shop.data, function(k, v) {
				$(".lov-shop").append($('<option>').text(v).attr('value', v));
			});
		},
	},

	pricelist: {
		data: ['', 'Pricelist Public (IDR)'],
		load: function() {
			$.each(listOfValues.pricelist.data, function(k, v) {
				$(".lov-pricelist").append($('<option>').text(v).attr('value', v));
			});
		},
	},

	status: {
		data: ['', 'OK', 'Received', 'Delivered', 'Shipped', 'Pending', 'Warning', 'Cancelled', 'Out of Stock'],
		load: function() {
			$.each(listOfValues.status.data, function(k, v) {
				$(".lov-status").append($('<option>').text(v).attr('value', v));
			});
		},
	},

	machine: {
		data: ['', 'M0-14', 'M0-51', 'M0-99', 'M1-10', 'M1-46', 'M1-47', 'M1-57', 'M1-64', 'M1-65', 'M2-23', 'M2-74', 'M3-27', 'M3-62', 'M4-16', 'M4-26', 'M5-61', 'M5-75', 'M5-75', 'M5-90', 'M6-34', 'M6-37', 'M6-47', 'M6-60', 'M7-21', 'M7-81', 'M7-87', 'M8-27', 'M8-38', 'M8-66', 'M8-84', 'M8-97', 'M9-14', 'M9-27', 'M9-38',],
		load: function() {
			$.each(listOfValues.machine.data, function(k, v) {
				$(".lov-machine").append($('<option>').text(v).attr('value', v));
			});
		},
	},

	worktype: {
		data: ['', 'Cutting', 'Machining', 'Harden'],
		load: function() {
			$.each(listOfValues.worktype.data, function(k, v) {
				$(".lov-work-type").append($('<option>').text(v).attr('value', v));
			});
		},
	},

	supplier: {
		data: ['', 'Daido Kogyo Co. Ltd.', 'Daido Die & Mold Steel Solutions, Co. Ltd.'],
		load: function() {
			$.each(listOfValues.supplier.data, function(k, v) {
				$(".lov-supplier").append($('<option>').text(v).attr('value', v));
			});
		},
	},

	customer: {
		data: data.customerCard.raw,
		name: {
			load: function() {
				$(".lov-customer").append($('<option>').text(''));
				$.each(listOfValues.customer.data, function(k, v) {
					$(".lov-customer").append($('<option>').text(v.name).attr('value', v.id));
				});
			},
			change: function() {
				var selected = $(".lov-customer").val();
				$.each(listOfValues.customer.data, function(k, v) {
					if(v.id == selected) {
						$(".lov-customer-address").append($('<option>').text(v.address).attr('value', v.address));
						$(".lov-customer-shipment-address").append($('<option>').text(v.address).attr('value', v.address));	
						$(".lov-customer-reference").val(v.reference);

					}
				});
			}
		},
	},

	workOrder: {
		data: data.workOrder.raw,
		load: function() {
			$(".lov-work-order").append($('<option>').text(''));
			$.each(listOfValues.workOrder.data, function(k, v) {
				$(".lov-work-order").append($('<option>').text(v.noWo).attr('value', v.id));
			});
		},
		change: function() {
			var selected = $(".lov-work-order").val();
			$.each(listOfValues.workOrder.data, function(k, v) {
				if(v.id == selected) {
					$(".type").val(v.status);
					$(".machine").val(v.machine);
					$(".datetime").val(v.datetime);

				}
			});
		}
	},

	salesOrder: {
		data: data.salesOrder.raw,
		noSo: {
			load: function() {
				$(".lov-sales-order").append($('<option>').text(''));
				$.each(listOfValues.salesOrder.data, function(k, v) {
					$(".lov-sales-order").append($('<option>').text(v.noSo).attr('value', v.id));
				});
			},
		},
	},
};
